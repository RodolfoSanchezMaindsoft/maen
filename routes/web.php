<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Cuenta
 */
Route::group(['middleware' => 'user'], function () {
    /*
     * Ruta donde se muestra la informacion de la empresa
     */
    Route::get('/usuario/mi-empresa','EnterpriseController@showMyEnterprise')->name('mi-empresa');

    Route::post('usuario/mi-empresa/updateEnterprise', 'EnterpriseController@updateEnterprise')->name('actualizar-empresa');
});


/*
 * Ruta para obtener los datos que se ingresan en el formulario del login
 */
Route::group(['middleware' => 'login'], function () {

    /*
    *  Ruta para mostrar el login donde debe iniciar sesión los usuarios
    */
    Route::get('/usuario/login', 'Auth\LoginController@showLoginForm')->middleware('guest');

});

Route::post('/usuario/login', 'Auth\LoginController@login')->name('login');


/**
 * ------------------------------------------------------------------------------------------------------------
 */

/*
 * Ruta que se muestra cuando se realiza el login de manera correcta.
Route::get('/usuario/dashboard', 'DashboardController@index')->name('dashboard');
*/

Route::get('/usuario/dashboard', 'Auth\UpdateEnterpriseController@showUpdateEnterpriseForm')->middleware('guest')->name('dashboard');

//Route::view('/usuario/dashboard','usuario.dashboard')->name('dashboard');

Route::post('/usuario/dashboard', 'Auth\UpdateEnterpriseController@update')->name('updateEnterprise');


Route::get('/usuario/logout', 'Auth\LoginController@logout')->name('logout');


/*

Route::get('/usuario/inicio', function () {
    return view('usuario.inicio');
});

Route::view('estampado-cnc', 'estampado-cnc')->name('estampado-cnc');
*/
