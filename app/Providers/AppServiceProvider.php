<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Como identificador agregaremos GuzzleHttp\Client y así le comunicamos a laravel que cada vez que
         * utilicemos GuzzleHttp\Client nos retorne una nueva instancia de la clase Client
         *
         */
        $this->app->singleton('GuzzleHttp\Client', function (){

            return new Client([
                // URL base para la petición
                'base_uri' => 'https://maen-pruebas.herokuapp.com/'
            ]);
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
