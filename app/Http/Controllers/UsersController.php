<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Users;

class UsersController extends Controller
{
    protected $users;

    public function __construct(Users $users)

    {
        $this->users = $users;

    }

    public function login($email,$contrasena){
        $usuario = $this->users->getClientAccessNormal($email, $contrasena);
        return $usuario;
    }
}
