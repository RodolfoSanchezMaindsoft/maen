<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\CertificationsController;
use App\Http\Controllers\EnterpriseController;
use App\Http\Controllers\MunicipalityController;
use App\Http\Controllers\SectorsController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UsersController;
use App\Repositories\Enterprise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UpdateEnterpriseController extends Controller
{
    protected $sectorsController;
    protected $statesController;
    protected $municipalityController;
    protected $certificationsController;
    protected $usersController;
    protected $enterpriseController;

    public function __construct(SectorsController $sectorsController,
                                StatesController $statesController, MunicipalityController $municipalityController,
                                CertificationsController $certificationsController, UsersController $usersController,
                                EnterpriseController $enterpriseController)

    {
        $this->middleware('guest');
        $this->sectorsController = $sectorsController;
        $this->statesController = $statesController;
        $this->municipalityController = $municipalityController;
        $this->certificationsController = $certificationsController;
        $this->usersController = $usersController;
        $this->enterpriseController = $enterpriseController;
    }

    public function showUpdateEnterpriseForm(Request $usuario){
        $nombre_usuario = $usuario['nombre_usuario'];
        $rfc_empresa = $usuario['rfc_empresa'];
        $sectores = $this->sectorsController->getSectors($rfc_empresa);
        $estados = $this->statesController->getStates($rfc_empresa);
        $municipios = $this->municipalityController->getMunicipality($rfc_empresa);
        $certificaciones = $this->certificationsController->getCertifications($rfc_empresa);
        $empresas = $this->enterpriseController->getEnterprise($rfc_empresa);
        return view('usuario.dashboard', compact('nombre_usuario','rfc_empresa','sectores','estados','municipios','certificaciones','empresas'));
    }

    public function update()
    {
        $credenciales = $this->validate(request(), [
            'razon_social' => 'required|string',
            'nombre_comercial' => 'required|string',
            'rfc' =>  ['required'],
            //'rfc' =>  ['required', 'regex:/^[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)([A-Z0-9]{4})$/'],
            'telefono_empresa' =>  ['required', 'digits:10'],
            'email_empresa' => 'email|required|string',
            //'ano_operaciones' => 'required|date',
            'horario_empresa' => 'required|string',
            'calle_domicilio' => 'required|string',
            'num_int_ext' => 'required|string',
            'colonia' => 'required|string',
            'codigo_postal' =>  ['required', 'digits:5'],
            'link_google_maps' =>  ['required', 'regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
            'descripcion_empresa' => 'required|string',
            'sitio_web' =>  ['required', 'regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
            'premios_obtenidos' => 'required|string',
            'num_empleados' => 'required|numeric',
            'crecimiento_anual' => 'required|numeric',
            'ventas_anuales' => 'required|numeric',
            'patentes_registradas' => 'required|numeric',
            //'facebook_empresa' =>  ['regex:/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/'],
            //'linkedin_empresa' =>  ['regex:/^(https?:\/\/)?(www\.)?linkedin.com\/[a-zA-Z0-9(\.\?)?]/'],

        ]);

        if ($credenciales = true) {
            $razon_social = Input::get('razon_social');
            $nombre_comercial = Input::get('nombre_comercial');
            $rfc = Input::get('rfc');
            $telefono_empresa = Input::get('telefono_empresa');
            $email_empresa = Input::get('email_empresa');
            //$ano_operaciones = Input::get('ano_operaciones');
            $horario_empresa = Input::get('horario_empresa');
            $calle_domicilio = Input::get('calle_domicilio');
            $num_int_ext = Input::get('num_int_ext');
            $colonia = Input::get('colonia');
            $codigo_postal = Input::get('codigo_postal');
            $link_google_maps = Input::get('link_google_maps');
            $descripcion_empresa = Input::get('descripcion_empresa');
            $sitio_web = Input::get('sitio_web');
            $premios_obtenidos = Input::get('premios_obtenidos');
            $num_empleados = Input::get('num_empleados');
            $crecimiento_anual = Input::get('crecimiento_anual');
            $ventas_anuales = Input::get('ventas_anuales');
            $patentes_registradas = Input::get('patentes_registradas');
            $facebook_empresa = Input::get('facebook_empresa');
            $linkedin_empresa = Input::get('linkedin_empresa');

            $empresa = $this->enterpriseController->updateEnterprise($razon_social,$nombre_comercial,$rfc,$telefono_empresa,$email_empresa,
                $horario_empresa,$calle_domicilio,$num_int_ext,$colonia,$codigo_postal,$link_google_maps,
                $descripcion_empresa,$premios_obtenidos,$num_empleados,$crecimiento_anual,$patentes_registradas,
                $facebook_empresa);

        }

        return back()
            ->withInput(request(['razon_social']));
    }

}
