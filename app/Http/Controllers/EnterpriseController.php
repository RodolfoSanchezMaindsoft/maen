<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Auth;
use App\Repositories\Enterprise;
use Illuminate\Http\Request;
use App\Http\Controllers\SectorsController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\MunicipalityController;

class EnterpriseController extends Controller
{
    protected $enterprise;
    protected $sectorsController;
    protected $statesController;
    protected $municipalityController;
    protected $certificationsController;

    public function __construct(Enterprise $enterprise,SectorsController $sectorsController,StatesController $statesController, MunicipalityController $municipalityController,CertificationsController $certificationsController)
    {
        $this->enterprise = $enterprise;
        $this->sectorsController = $sectorsController;
        $this->statesController = $statesController;
        $this->municipalityController = $municipalityController;
        $this->certificationsController = $certificationsController;
    }

    public function getEnterprise($rfc_empresa){
        $empresa = $this->enterprise->getEnterprise($rfc_empresa);
        return $empresa;
    }

    public function updateEnterprises($razon_social,$nombre_comercial,$rfc,$telefono_empresa,$email_empresa,
                                     $horario_empresa,$calle_domicilio,$num_int_ext,$colonia,$codigo_postal,$link_google_maps,
                                     $descripcion_empresa,$premios_obtenidos,$num_empleados,$crecimiento_anual,$patentes_registradas,
                                     $facebook_empresa){


        $empresa = $this->enterprise->getClientAccessNormal($razon_social,$nombre_comercial,$rfc,$telefono_empresa,$email_empresa,
            $horario_empresa,$calle_domicilio,$num_int_ext,$colonia,$codigo_postal,$link_google_maps,
            $descripcion_empresa,$premios_obtenidos,$num_empleados,$crecimiento_anual,$patentes_registradas,
            $facebook_empresa);

        return $empresa;


    }

    public function showMyEnterprise(){
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];
        $rfc_empresa = $perfil_usuario->RFC_EMPRESA;
        $empresas = $this->getEnterprise($rfc_empresa);
        $sectores = $this->sectorsController->getSectors($rfc_empresa);
        $estados = $this->statesController->getStates($rfc_empresa);
        $municipios = $this->municipalityController->getMunicipality($rfc_empresa);
        $certificaciones = $this->certificationsController->getCertifications($rfc_empresa);
        //dd($empresa);
        return view('usuario.mi-empresa', compact('empresas','sectores','estados','municipios','certificaciones'));
    }


    public function updateEnterprise(Request $request){

        $credenciales = $this->validate(request(), [
            'razon_social' => 'required|string',
            'nombre_comercial' => 'required|string',
            'telefono_empresa' =>  ['required', 'digits:10'],
            'email_empresa' => 'email|required|string',
            'horario_empresa' => 'required|string',
            'calle_domicilio' => 'required|string',
            'num_int_ext' => 'required|string',
            'colonia' => 'required|string',
            'codigo_postal' =>  ['required', 'digits:5'],
            'link_google_maps' =>  ['required', 'regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
            'descripcion_empresa' => 'required|string',
            'premios_obtenidos' => 'required|string',
            'num_empleados' => 'required|numeric',
            'crecimiento_anual' => 'required|numeric',
            'ventas_anuales' => 'required|numeric',
            'patentes_registradas' => 'required|numeric',

        ]);

        if ($credenciales = true) {
            $rfc_empresa = $request->id;
            $razon_social = $request->razon_social;
            $nombre_comercial = $request->nombre_comercial;
            $telefono_empresa = $request->telefono_empresa;
            $email_empresa = $request->email_empresa;
            $horario_empresa = $request->horario_empresa;
            $sectores = $request->sectores;
            $municipio = $request->municipio;
            $calle_domicilio = $request->calle_domicilio;
            $num_int_ext = $request->num_int_ext;
            $colonia = $request->colonia;
            $codigo_postal = $request->codigo_postal;
            $link_google_maps = $request->link_google_maps;
            $descripcion_empresa = $request->descripcion_empresa;
            $premios_obtenidos = $request->premios_obtenidos;
            $certificaciones = $request->certificaciones;
            $num_empleados = $request->num_empleados;
            $crecimiento_anual = $request->crecimiento_anual;
            $ventas_anuales = $request->ventas_anuales;
            $patentes_registradas = $request->patentes_registradas;
            $facebook_empresa =! null ? $request->facebook_empresa : " ";
            $youtube_empresa =! null ? $request->youtube_empresa : " ";
            $linkedin_empresa =! null ? $request->linkedin_empresa : " ";
            $twitter_empresa =! null ? $request->twitter_empresa : " ";
            $instagram_empresa =! null ? $request->instagram_empresa : " ";
            $is_update = $this->enterprise->updateEnterprise($rfc_empresa,$razon_social,$nombre_comercial,$telefono_empresa,$email_empresa,$horario_empresa,$municipio,$calle_domicilio,$num_int_ext,$colonia,$codigo_postal,$link_google_maps,$descripcion_empresa,$premios_obtenidos,$num_empleados,$crecimiento_anual,$ventas_anuales,$patentes_registradas,$facebook_empresa,$youtube_empresa,$linkedin_empresa,$twitter_empresa,$instagram_empresa);
            if ($is_update->response == 1){
                $empresas = $this->enterprise->getEnterprise($rfc_empresa);
                $sectores = $this->sectorsController->updateSectors($rfc_empresa,$sectores);
                $sectores = $this->sectorsController->getSectors($rfc_empresa);
                $estados = $this->statesController->getStates($rfc_empresa);
                $municipios = $this->municipalityController->getMunicipality($rfc_empresa);
                $certificaciones = $this->certificationsController->updateCertifications($rfc_empresa,$certificaciones);
                $certificaciones = $this->certificationsController->getCertifications($rfc_empresa);
                $vista  = view('componentes.empresa' , compact('empresas', 'sectores' ,'estados','municipios','certificaciones'))->render();
                return response()->json(['html'=>$vista]);
            }
        }

    }
}
