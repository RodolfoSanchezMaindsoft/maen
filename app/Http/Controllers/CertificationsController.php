<?php

namespace App\Http\Controllers;

use App\Repositories\Certifications;
use Illuminate\Http\Request;

class CertificationsController extends Controller
{
    protected $certifications;

    public function __construct(Certifications $certifications)
    {
        $this->certifications = $certifications;
    }

    public function getCertifications($rfc_empresa){
        $certificaciones = $this->certifications->getCertifications($rfc_empresa);
        return $certificaciones;
    }

    public function updateCertifications($rfc_empresa,$certificaciones_actualizar){
        $this->certifications->updateCertifications($rfc_empresa,$certificaciones_actualizar);
    }
}
