<?php

namespace App\Http\Middleware;

use Closure;

class UsersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('perfilUsuario')){
            $perfil_usuario = session()->get('perfilUsuario');
            foreach ($perfil_usuario as $usuario) {
                if (print_r($usuario->NOMBRE, true) == null) {
                    return redirect('/usuario/login');
                }
            }
        }else{
            return redirect('/usuario/login');
        }

        return $next($request);
    }
}
