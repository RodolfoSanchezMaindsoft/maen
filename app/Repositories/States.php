<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/6/2019
 * Time: 3:27 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class States
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getStates($rfc_empresa){

        $response = $this->client->request('POST', '/enterprise/getAllCatalogs',
            [  "json" =>
                ['RFC_EMPRESA' => "$rfc_empresa",
                    'PLATAFORMA' => 'WEB']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $estados = json_decode($response->getBody()->getContents());

        $estados = $estados->data->data->ESTADOS;

        //dd($estados);

        return $estados;
    }

}