<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/7/2019
 * Time: 3:57 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Enterprise
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public  function getEnterprise($rfc_empresa){
        $response = $this->client->request('POST', '/enterprise/getAllCatalogs',
            [  "json" =>
                ['RFC_EMPRESA' => "$rfc_empresa",
                    'PLATAFORMA' => 'WEB']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $empresa = json_decode($response->getBody()->getContents());

        $empresa = $empresa->data->data->DATOS_EMPRESA;

        //dd($empresa);

        return $empresa;
    }

    public function updateEnterprise($rfc_empresa,$razon_social,$nombre_comercial,$telefono_empresa,$email_empresa,$horario_empresa,$municipio,$calle_domicilio,$num_int_ext,$colonia,$codigo_postal,$link_google_maps,$descripcion_empresa,$premios_obtenidos,$num_empleados,$crecimiento_anual,$ventas_anuales,$patentes_registradas,$facebook_empresa
        ,$youtube_empresa,$linkedin_empresa,$twitter_empresa,$instagram_empresa){
        $response = $this->client->request('POST', '/enterprise/updateEnterprise',
            [  "json" =>
                    ['RAZON_SOCIAL' => "$razon_social",
                     'NOMBRE_COMERCIAL' => "$nombre_comercial",
                     'RFC_EMPRESA' => "$rfc_empresa",
                     'TELEFONO' => "$telefono_empresa",
                     'CORREO_ELECTRONICO' => "$email_empresa",
                     'HORARIO_ATENCION' => "$horario_empresa",
                     'FECHA_INICIO_OPERACIONES' => "01/01/1990",
                     'ID_PAIS' => "1",
                     'ID_ESTADO' => "1",
                     'ID_MUNICIPIO' => "$municipio",
                     'CALLE' => "$calle_domicilio",
                     'NUM_INT' => "$num_int_ext",
                     'NUM_EXT' => "",
                     'COLONIA' => "$colonia",
                     'CODIGO_POSTAL' => "$codigo_postal",
                     'LINK_GOOGLEMAPS' => "$link_google_maps",
                     'LATITUD' => "",
                     'LONGITUD' => "",
                     'DESCRIPCION_BREVE' => "$descripcion_empresa",
                     'PREMIOS_CALIDAD' => "$premios_obtenidos",
                     'CANTIDAD_EMPLEADOS' => "$num_empleados",
                     'CRECIMIENTO_ANUAL' => "$crecimiento_anual",
                     'PROMEDIO_VENTA_ANUAL' => "$ventas_anuales",
                     'PATENTES_REGISTRADAS' => "$patentes_registradas",
                     'LINK_FACEBOOK' => "$facebook_empresa",
                     'LINK_YOUTUBE' => "$youtube_empresa",
                     'LINK_LINKEDIN' => "$linkedin_empresa",
                     'LINK_TWITTER' => "$twitter_empresa",
                     'LINK_INSTAGRAM' => "$instagram_empresa",
            ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $empresa = json_decode($response->getBody()->getContents());

        //Mandamos a llamar Data->Data ya que el JSON es de 3 níveles y cada data es un nivel al que vamos accediendo
        $empresa = $empresa->data;

        return $empresa;

    }

    public function getClientAccessNormal($razon_social,$nombre_comercial,$rfc,$telefono_empresa,$email_empresa,
                                          $horario_empresa,$calle_domicilio,$num_int_ext,$colonia,$codigo_postal,$link_google_maps,
                                          $descripcion_empresa,$premios_obtenidos,$num_empleados,$crecimiento_anual,$patentes_registradas,
                                          $facebook_empresa){

        $mensaje_error = "Usuario o contraseña invalidos";

        $response = $this->client->request('POST', '/enterprise/updateEnterprise',
            [  "json" =>
                ['RAZON_SOCIAL' => "$razon_social",
                 'NOMBRE_COMERCIAL' => "$nombre_comercial",
                 'RFC_EMPRESA' => "$rfc",
                 'TELEFONO' => "$telefono_empresa",
                 'CORREO_ELECTRONICO' => "$email_empresa",
                 //'FECHA_INICIO_OPERACIONES' => "",
                 'ID_PAIS' => "1",
                 'ID_ESTADO' => "1",
                 'ID_MUNICIPIO' => "1",
                 'HORARIO_ATENCION' => "$horario_empresa",
                 'CALLE' => "$calle_domicilio",
                 'NUM_INT' => "$num_int_ext",
                 'NUM_EXT' => "",
                 'COLONIA' => "$colonia",
                 'CODIGO_POSTAL' => "$codigo_postal",
                 'LINK_GOOGLEMAPS' => "$link_google_maps",
                 'LATITUD' => "",
                 'LONGITUD' => "",
                 'DESCRIPCION_BREVE' => "$descripcion_empresa",
                 'PREMIO_CALIDAD' => "$premios_obtenidos",
                 'CANTIDAD_EMPLEADOS' => "$num_empleados",
                 'CRECIMIENTO_ANUAL' => "$crecimiento_anual",
                 'PROMEDIO_VENTA_ANUAL' => "0",
                 'PATENTES_REGISTRADAS' => "$patentes_registradas",
                 'LINK_FACEBOOK' => "$facebook_empresa",
                ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $empresa = json_decode($response->getBody()->getContents());


        //Mandamos a llamar Data->Data ya que el JSON es de 3 níveles y cada data es un nivel al que vamos accediendo
        $empresa = $empresa->data;

        dd($empresa);

        if ($empresa == $mensaje_error){

            return  $mensaje_error;
        }

        else{

            dd($empresa);

            return $empresa;

        }

    }
}