<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/6/2019
 * Time: 10:28 AM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Users
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getClientAccessNormal($email,$contrasena){

        $mensaje_error = "Usuario o contraseña invalidos";

        $response = $this->client->request('POST', '/enterprise/login',
            [  "json" =>
                    ['EMAIL' => "$email",
                    'CONTRASENA' => "$contrasena",
                    'PLATAFORMA' => 'WEB']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());


        //Mandamos a llamar Data->Data ya que el JSON es de 3 níveles y cada data es un nivel al que vamos accediendo
        $usuario = $usuario->data->data;

        if ($usuario == $mensaje_error){

            return  $mensaje_error;
        }

        else{

            //dd($usuario);

            return $usuario;

        }

    }
}