<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/6/2019
 * Time: 5:04 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Certifications
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getCertifications($rfc_empresa){

        $response = $this->client->request('POST', '/enterprise/getAllCatalogs',
            [  "json" =>
                ['RFC_EMPRESA' => "$rfc_empresa",
                    'PLATAFORMA' => 'WEB']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $certificaciones = json_decode($response->getBody()->getContents());

        $certificaciones = $certificaciones->data->data->CERTIFICACIONES;

        //dd($certificaciones);

        return $certificaciones;
    }

    public function updateCertifications($rfc_empresa,$certificaciones_actualizar){
        $response = $this->client->request('POST', '/enterprise/insertCertifications',
            [  "json" =>
                ['RFC_EMPRESA' => "$rfc_empresa",
                    'CERTIFICACIONES' => "$certificaciones_actualizar",
                    'PLATAFORMA' => 'WEB']]);

    }

}