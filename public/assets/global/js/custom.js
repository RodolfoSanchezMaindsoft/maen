/**
 * custom.js
 * Autor: Luis Cristerna (Maindsoft)
 * Fecha: 25/02/2018
 */

/**
 * Script con el que funciona el traductor gtranslate
 */
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,hide_arrows,hide_arrows);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */


/**
 * Funcion que nos ayuda a ocultar las flechas del revolution slider al momento dw pasar el mouse por contenido del mega-menu,
 * se tuvo que agregar el atributo id="leftarrow" y id="rightarrow" en el archivo assets/global/js/revolution-slider/extensions/source/revolution.extension.navigation.js
 * en la linea 831 y 833 respectivamente para que funcione de manera correcta
 */
function hide_arrows() {


    jQuery('#mega-menu-div').mouseover(function() {
        $('#leftarrow').hide();
    });

    jQuery('#mega-menu-label-1').mouseover(function() {
        $('#leftarrow').hide();
    });

    jQuery('#mega-menu-div').mouseout(function() {
        $('#leftarrow').show();
    });

    jQuery('#mega-menu-label-1').mouseout(function() {
        $('#leftarrow').show();
    });

    jQuery('#mega-menu-div').mouseover(function() {
        $('#rightarrow').hide();
    });

    jQuery('#mega-menu-label-1').mouseover(function() {
        $('#rightarrow').hide();
    });

    jQuery('#mega-menu-div').mouseout(function() {
        $('#rightarrow').show();
    });

    jQuery('#mega-menu-label-1').mouseout(function() {
        $('#rightarrow').show();
    });

}

/**
 * Funcion que nos permite tener una barra de busqueda con autocompletado,
 * los datos se llenan por medio de una base de datos que se llama por medio del
 * archivo buscar.php, ademas se le agrega minLength, para poner el minimo
 * de caracteres para que empiece la busqueda.
 */
function search_bar_autocomplete() {

    $(function() {
        $(document.getElementById('inp-sector-auto-maen')).autocomplete({
            source: "/php/buscar.php",
            minLength: 2
        });

        $(document.getElementById('inp-sector-auto-maen-movil')).autocomplete({
            source: "/php/buscar.php",
            minLength: 2
        });

        $(document.getElementById('input-nombre-servicio-auto')).autocomplete({
            source: "/php/buscar.php",
            minLength: 0
        });

    });
}

function change_page() {
    location.href = '/estampado-cnc';
}

function search_bar_modal() {
    $(function() {

        $(document.getElementById('input-nombre-modal-servicio-auto')).autocomplete({
            source: "php/buscar.php",
            minLength: 0
        });
    });
}

/**
 * Funcion  para ocultar la imagen al momento que la barra de busqueda se este utilizando.
 */
function search_bar_mobile_focus() {
    $('#img-logo-header').hide();
}

/**
 * Funcion  para mostrar la imagen al momento que la barra de busqueda no se este utilizando.
 */
function search_bar_mobile_onblur() {
    $('#img-logo-header').show();
}

var id_anterior;
function change_id_panel_header(id) {
    if (id_anterior == undefined) {
        document.getElementById("comercio_").id = "comercio_" + id;

    }else if (id_anterior != undefined){
        document.getElementById("comercio_" + id_anterior).id = "comercio_" + id;
    }

    id_anterior = id;

}

var id_anterior_producto_movil;
var id_anterior_comercio_movil;
function change_id_panel_header_mobile(id_commerce, id_product) {
    if (id_anterior_producto_movil == undefined) {
        document.getElementById("nav-submenu-").id = "nav-submenu-" + id_commerce + "-" + id_product;
        document.getElementById("nav-submenu-" + id_commerce + "-" + id_product).setAttribute("aria-labelledby", "nav-link-" + id_commerce + "-" + id_product);


    }else if (id_anterior_producto_movil != undefined){
        document.getElementById("nav-submenu-" + id_anterior_comercio_movil + "-" + id_anterior_producto_movil).id = "nav-submenu-" + id_commerce + "-" + id_product;
        document.getElementById("nav-submenu-" + id_commerce + "-" + id_product).setAttribute("aria-labelledby", "nav-link-" + id_commerce + "-" + id_product);

    }

    id_anterior_comercio_movil = id_commerce;
    id_anterior_producto_movil = id_product;

}

function get_mega_menu_sub_items(id) {
    var url= "php/megamenu.php";
    $.ajax({
        url: url,
        type: 'POST',
        data: 'id=' + id,
        success: function (resultado) {
            $('#megamenu-sub-items').html(resultado);
        }
    });
}

function get_sub_menu(tittle_commerce,id_commerce,id_product) {
    var url= "php/submenu.php";
    $.ajax({
        url: url,
        type: 'POST',
        data: { tittle_commerce: tittle_commerce, id_commerce: id_commerce, id_product: id_product },
        success: function (resultado) {
            $('#nav-submenu-'  + id_commerce + "-" + id_product).html(resultado);
        }
    });
}

function link_3_levels(page, category, type, product) {
    var link = page + '/' + category + "/" + type + "/" + product;
    window.location.href = link;
}

function link_2_levels(page, category, product) {
    var link = page + '/' + category +  "/" + product;
    window.location.href = link;
}

function link_1_level(page, category) {
    var link = page + '/' + category;
    window.location.href = link;
}

function obtain_text_search_bar(type_search_bar) {
    if (type_search_bar === "Mobile") {
        text =  $('#inp-sector-auto-maen-movil').val();
    }
    else if (type_search_bar === "Desktop") {
        text =  $('#inp-sector-auto-maen').val();
    }

    if (validate_text_search_bar(text) == true) {
        var url= "php/searchBar.php";
        $.ajax({
            url: url,
            type: 'POST',
            data: 'text=' + text,
            success: function (resultado) {
                if (resultado == 1) {
                    alert("No hay publicaciones que coincidan con tu búsqueda.")
                }else {
                    window.location.href = resultado;
                }
            }
        });
    }

}

function validate_text_search_bar(text) {
    if (is_campo_corto(text,"Barra De Busqueda",2) === false) {
        return false;
    }else {
        return true;
    }

}

/**
 * Metodo que obtiene todos los datos necesarios para enviar un correo sobre alguna inquietud especifica de un usuario
 */
function obtener_datos_correo() {
    inp_nombre = $('#inp-nombre').val();
    inp_mensaje = $('#txta-mensaje').val();
    inp_correo = $('#inp-correo').val();
    inp_telefono = $('#inp-telefono').val();
    select_asunto = $('#inp-asunto').val();
    validar_campos(inp_nombre,inp_mensaje, inp_correo, inp_telefono, select_asunto);
}

/**
 * Metodo que valida los campos que contiene el formulario, si algun campo es incorrecto
 * mandara la alerta con el error que se tiene, si ningun campo es incorrecto entonces se llamara
 * el metodo enviar_datos_correo
 * @param nombre
 * @param mensaje
 * @param correo
 * @param telefono
 * @param asunto
 */
function validar_campos(nombre, mensaje, correo, telefono, asunto) {

    if (is_campo_vacio(mensaje, "Mensaje") == false || is_campo_corto(mensaje, "Mensaje", 10) == false||
        is_campo_vacio(nombre, "Nombre") == false || is_campo_corto(nombre, "Nombre", 3) == false ||
        is_campo_vacio(correo, "Correo") == false || is_email(correo, "Correo") == false ||
        is_campo_vacio(telefono, "Telefono") == false || is_telefono(telefono, "Telefono") == false ||
        is_campo_vacio(asunto, "Asunto") == false || is_campo_corto(asunto, "Asunto", 4) == false
    ){
    } else {
        enviar_datos_correo(nombre,mensaje, correo, telefono, asunto);
    }
}

/**
 * Metodo que obtiene los datos que contiene el formulario de contacto.php,
 * despues de ello enviara dichos datos a la pagina enviar-correo.php.
 * @param nombre
 * @param mensaje
 * @param correo
 * @param telefono
 * @param asunto
 */
function enviar_datos_correo(nombre,mensaje, correo, telefono, asunto) {
    var datos_cotizacion = new FormData();
    datos_cotizacion.append('NOMBRE',nombre);
    datos_cotizacion.append('MENSAJE',mensaje);
    datos_cotizacion.append('CORREO',correo);
    datos_cotizacion.append('TELEFONO',telefono);
    datos_cotizacion.append('ASUNTO',asunto);
    $.ajax({
        url: 'php/enviar-correo.php',
        type: 'post',
        data:  datos_cotizacion,
        contentType: false,
        processData: false,
        success: function (resultado) {
            if (resultado == 1) {
                alert("Estimado usuario:\n" +
                    "\n" +
                    "Agradecemos tu interes en conocer de cerca nuestros servicios, Grupo Industrial Estrada " +
                    "revisara tus datos y se pondran en contacto muy pronto contigo.");
                location.reload();
            } else {
                alert(resultado);
            }
        }
    });

}

function obtener_datos_cotizacion(url_producto,nivel) {
    $('#resultado').show();
    nombre = $("#inp-nombre-cliente-empresa").val();
    email = $("#inp-email-cliente-empresa").val()
    telefono = $("#inp-movil-cliente-empresa").val();
    descripcion = $("#txta-comentario-cliente-empresa").val();
    url_pagina = window.location.href;
    if (is_campo_vacio(nombre, "Nombre") == false || is_campo_corto(nombre, "Nombre", 2) == false ||
        is_campo_largo(nombre,"Nombre", 100) == false ||
        is_email(email, "Email") == false || is_telefono(telefono, "Teléfono de 10 Digitos") == false ||
        is_campo_largo(descripcion,"Comentario adicional", 250) == false){
    }else {
        var datos_cotizacion = new FormData();
        datos_cotizacion.append('URL', url_producto);
        datos_cotizacion.append('NIVEL', nivel);
        datos_cotizacion.append('NOMBRE', nombre);
        datos_cotizacion.append('EMAIL', email);
        datos_cotizacion.append('TELEFONO', telefono);
        datos_cotizacion.append('DESCRIPCION', descripcion);
        datos_cotizacion.append('URL_PAGINA', url_pagina);
        $.ajax({
            data: datos_cotizacion, //datos que se envian a traves de ajax
            url: 'php/enviar-cotizacion.php', //archivo que recibe la peticion
            type: 'post', //método de envio
            contentType: false,
            processData: false,
            beforeSend: function () {
                $("#resultado").html('<div class="alert alert-info alert-dismissible" role="alert"> ' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> ' +
                    '<strong>Enviando!</strong> Espere un momento' +
                    '</div> ');
            },
            success: function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                if ($('#modal-cotizacion-especifica').length){
                    borrar_modal_cotizacion();
                    $("#resultado_cotizacion").html(response);
                    $('#modal-cotizacion-especifica').modal('hide');
                } else if ($('#modal-cotizacion-general').length){
                    borrar_modal_cotizacion();
                    $("#resultado_cotizacion_general").html(response);
                    $('#modal-cotizacion-general').modal('hide');
                }
            }
        });
    }
}

function borrar_modal_cotizacion() {
    $('#modalQuote').modal('hide');
    $('#resultado').hide();
    $('#form-cotizacion').trigger("reset");
}

var id_anterior_modal_producto;
var nombre_modal_producto;
function change_id_modal(modal_id) {
    if ($('#modalQuote').length){
        document.getElementById("modalQuote").id = modal_id;
        $('#' + modal_id).modal('show');
    } else {
        document.getElementById(id_anterior_modal_producto).id = modal_id;
        $('#' + modal_id).modal('show');
    }

    id_anterior_modal_producto = modal_id;
}