<!--
    * Nombre: estampado-cnc.php
    * Creado por: Luis Cristerna (MAINDSOFT SISTEMAS INTEGRALES)
    * Fecha: 02/07/2019
-->

<!DOCTYPE html>
<html lang="es" class="font-primary">

<head>

    <title>MAEN | Portal Administración | Inicio</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Tag para que que los rastreadores web de la mayoría de los motores de búsqueda indexen esta página-->
    <meta name="robots" content="noindex">
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="keywords" content="Grupo,MAEN,Aguascalientes">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta name="description"
          content="NA">
    <!--Meta imagen (Imagen que se muestra al compartir la pagina)-->
    <meta name="image" content="NA">
    <!--Color de la pestana de la pagina (Utilizada en chrome para moviles, por ejemplo)-->
    <meta name="theme-color" content="#ED1C24">
    <!-- Meta Tags (Facebook, Pinterest & Google+) -->
    <!--Titulo de la pagina-->
    <meta property="og:title" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta property="og:description"
          content="NA">
    <!--URL de la pagina-->
    <meta property="og:url" content="https://grupomaen.mx/">
    <!--Nombre de la pagina-->
    <meta property="og:site_name" content="Grupo MAEN">
    <!--De que pais es la pagina, mas no el servidor-->
    <meta property="og:locale" content="es_MX">
    <!--Tipo de la pagina: sitio web-->
    <meta property="og:type" content="website"/>
    <!--Imagen al compartir-->
    <meta property="og:image" content="NA"/>
    <!--ID FB-->
    <meta property="fb:app_id" content="NA"/>
    <!-- Meta Tags Google -->
    <!--Titulo de la pagina-->
    <meta itemprop="name" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta itemprop="description"
          content="NA">

    <!-- Fuentes necesarias para la pagina -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600">
    <!-- Fin de fuentes -->

    <!-- Fav-->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/global/css/bootstrap/bootstrap.min.css">

    <!-- jquery -->
    <link rel="stylesheet" href="assets/global/css/jquery/jquery-ui.css">

    <!-- Icons -->
    <link rel="stylesheet" href="assets/global/css/icons/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/global/css/icons/icon-hs/style.css">

    <!-- header -->
    <link rel="stylesheet" href="assets/global/css/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="assets/global/css/megamenu/hs.megamenu.css">

    <!-- Animate -->
    <link rel="stylesheet" href="assets/global/css/animate/animate.css">

    <!-- Unify -->
    <link rel="stylesheet" href="assets/global/css/unify/unify.min.css">

    <!-- custom -->
    <link rel="stylesheet" href="assets/global/css/custom.css">

    <!-- End CSS -->

</head>

<body>
<main>

    <!-- Header -->
    <header id="js-header" class="u-header u-header--sticky-top u-header--toggle-section u-header--change-appearance " data-header-fix-moment="200" data-header-fix-effect="slide">
        <!-- Header Parte Arriba -->
        <div class="text-center text-lg-left u-header__section u-header__section--hidden u-header__section--light g-bg-white g-brd-bottom g-brd-gray-light-v4 g-py-5 g-pt-10 g-hidden-md-down">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-4 g-hidden-sm-down text-center">
                        <!-- Logo -->
                        <a href="javascript:;" class="js-go-to navbar-brand u-header__logo" data-type="static">
                            <img class="u-header__logo-img u-header__logo-img--main g-width-140" src="assets/global/img/logo/logo-maen.png" alt="Logo | Grupo Maen">
                        </a>
                        <!-- Fin Logo -->
                    </div>
                    <!-- La clase auto-widget junto con el input inp-sector-auto-maen son los necesarios para tener el autocompletado-->
                    <div class="col-xl-6 col-lg-5 g-hidden-md-down auto-widget g-mt-10">
                        <!-- Form -->
                        <form class="input-group rounded">
                            <!-- input inp-sector-auto-maen -->
                            <input id="inp-sector-auto-maen" class="form-control g-brd-secondary-light-v2 g-brd-primary--focus g-color-secondary-dark-v1 g-placeholder-secondary-dark-v1 g-bg-white g-font-weight-400 g-font-size-13 g-px-20 g-py-12" type="text" placeholder="Buscar producto, proceso, servicio o palabra clave">
                            <span class="input-group-append g-brd-none g-py-0 g-pr-0">
                                <button onclick="change_page();" class="btn u-btn-white g-color-primary--hover g-bg-secondary g-font-weight-600 g-font-size-13 text-uppercase g-py-12 g-px-20" type="button" >
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            <!-- Fin input inp-sector-auto-maen -->
                        </form>
                        <!-- Fin Form -->
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 text-center g-hidden-xs-down g-mt-10">
                        <div class="g-pa-10">
                            <li class="list-inline">
                                <a class="dropdown-toggle no-text-underline g-color-gray-dark-v3" data-toggle="dropdown" href="javascript:;">
                                    <span class="fa fa-globe g-color-primary"></span>
                                    Idioma
                                </a>
                                <!-- Con el evento onclick mandamos llamar la funcion doGTranslate del script dado por gtranslate y el cual
                                         tenemos en el archivo custom.js. Ademas debe de llevar como parametro el lenguaje al que queremos cambiar
                                         al momento de dar click-->
                                <ul class="dropdown-langs dropdown-menu">
                                    <li>
                                        <a class="g-color-gray-dark-v3" tabindex="-1" href="javascript:;" onclick="doGTranslate('es|es');">
                                            <img id="leng-esp" src="assets/global/img/banderas/mx.ico" width="16" height="11" alt="Español"/>
                                            Español
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a class="g-color-gray-dark-v3" tabindex="-1" href="javascript:;" onclick="doGTranslate('es|en');">
                                            <img id="leng-ing" src="assets/global/img/banderas/us.ico" width="16"
                                                 height="11" alt="English"/>
                                            Inglés
                                        </a>
                                    </li>
                                </ul>
                                <div id="google_translate_element2"></div>
                                <!-- Estilo necesario para el traductor -->
                                <style type="text/css">
                                    a.gflag{vertical-align:middle;font-size:24px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/24.png)}a.gflag img{border:0}a.gflag:hover{background-image:url(//gtranslate.net/flags/24a.png)}#goog-gt-tt{display:none!important}.goog-te-banner-frame{display:none!important}.goog-te-menu-value:hover{text-decoration:none!important}body{top:0!important}#google_translate_element2{display:none!important}
                                </style>
                                <!-- Fin Estilo necesario para el traductor -->
                                <!-- Scripts necesario para el traductor -->
                                <script type="text/javascript">function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'es',autoDisplay: false}, 'google_translate_element2');}</script>
                                <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>
                                <!-- Fin Scripts necesario para el traductor -->
                            </li>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 text-center g-mt-10">
                        <div class="g-pa-10">
                            <a href="javascript:;" class="g-color-gray-dark-v5 g-color-primary--hover"><span>únete ahora</span></a>
                            <span>/</span>
                            <a href="javascript:;" class="g-color-gray-dark-v5 g-color-primary--hover g-brd-black"><span> Inicia sesión</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin Header Parte Arriba -->

        <!-- Header Parte Abajo -->
        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10" data-header-fix-moment-exclude="g-bg-white g-py-10" data-header-fix-moment-classes="g-bg-white-opacity-0_7 u-shadow-v18 g-py-0">
            <nav class="js-mega-menu navbar navbar-expand">
                <!-- Boton Responsivo Móvil -->
                <button class="col-2 navbar-toggler navbar-toggler-left btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-10
		                    g-right-12" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                            <span class="hamburger hamburger--slider">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </span>
                </button>
                <!-- Fin Boton Responsivo Móvil -->
                <!-- Logo Móvil -->
                <div class="navbar-brand col-10 g-hidden-lg-up auto-widget">
                    <!-- Form -->
                    <form class="input-group rounded">
                        <a id='img-logo-header' href="#!" class="navbar-brand g-mr-35">
                            <img src="assets/global/img/logo/logo-maen-movil.png" alt="Logo Maen" height='26'>
                        </a>
                        <!-- input inp-sector-auto-maen -->
                        <input id="inp-sector-auto-maen-movil" onfocus='search_bar_mobile_focus()' onblur='search_bar_mobile_onblur()' class="form-control g-brd-secondary-light-v2 g-brd-primary--focus g-color-secondary-dark-v1 g-placeholder-secondary-dark-v1 g-bg-white g-font-weight-400 g-font-size-13 g-px-20 g-py-12" type="text" placeholder="Buscar...">
                        <button onclick='obtain_text_search_bar("Mobile")' id='btn-buscar-header' class="btn u-btn-white g-color-primary--hover g-bg-secondary g-font-weight-600 g-font-size-13 text-uppercase g-py-12 g-px-20" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                    <!-- Fin Form -->
                </div>
                <!-- Fin Logo Móvil -->

                <!-- Navegación -->
                <div class="js-navigation navbar-collapse align-items-center flex-sm-row u-main-nav--push-hidden g-pt-10 g-pt-5--lg g-mr-40--sm"
                     id="navBar" data-navigation-breakpoint="lg" data-navigation-position="left" data-navigation-init-classes="g-transition-0_5"
                     data-navigation-init-body-classes="g-transition-0_5" data-navigation-overlay-classes="g-bg-black-opacity-0_8 g-transition-0_5">
                    <div class="u-main-nav__list-wrapper mr-auto g-overflow-y-auto-lg">
                        <ul class="navbar-nav text-uppercase g-font-weight-600 mr-auto">
                            <li class="nav-item g-mx-20--lg">
                                <a href="javascript:void(0);" class="nav-link px-0">INICIO</a>
                            </li>
                            <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                                <a href="#!" class="nav-link g-px-0" id="nav-link-1" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-1">Nosotros</a>
                                <!-- Submenu -->
                                <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-1" aria-labelledby="nav-link-1">
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Grupo Maen</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Comisiones</a>
                                    </li>
                                </ul>
                                <!-- Fin Submenu -->
                            </li>

                            <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                                <a href="#!" class="nav-link g-px-0" id="nav-link-2" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-2">Productos</a>
                                <!-- Submenu -->
                                <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-2">
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Exhibidores</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Mostradores</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Alarmas</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Extintores</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Jaladeras</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Cofres para Tractos</a>
                                    </li>
                                </ul>
                                <!-- Fin Submenu -->
                            </li>

                            <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                                <a href="#!" class="nav-link g-px-0" id="nav-link-3" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-3">Procesos</a>
                                <!-- Submenu -->
                                <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-3">
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Estampado CNC</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Doblado de Tubo</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Corte Láser</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Acabados Metálicos</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Formado de Alambre</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Inyección de Plástico</a>
                                    </li>
                                </ul>
                                <!-- Fin Submenu -->
                            </li>

                            <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                                <a href="#!" class="nav-link g-px-0" id="nav-link-4" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-4">Servicios</a>
                                <!-- Submenu -->
                                <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-4">
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Instalación de Alarmas </a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Desarrollo de Apps.</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Curso para Técnicos</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Partes Estampadas</a>
                                    </li>
                                </ul>
                                <!-- Fin Submenu -->
                            </li>

                            <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                                <a href="#!" class="nav-link g-px-0" id="nav-link-5" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-5">Empresas</a>
                                <!-- Submenu -->
                                <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-5">
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Maindsteel</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Maindsoft</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Metal Stamping</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">SOIN3</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">QMC</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a class="nav-link g-px-0" href="#!">Maindsteel Automotive</a>
                                    </li>
                                </ul>
                                <!-- Fin Submenu -->
                            </li>

                            <li class="nav-item g-mx-20--lg">
                                <a href="javascript:void(0);" class="nav-link px-0">Blog</a>
                            </li>
                            <li class="nav-item g-mx-20--lg">
                                <a href="javascript:void(0);" class="nav-link px-0">Contacto</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Fin Navegación -->
            </nav>
        </div>
        <!-- Fin Header Parte Abajo -->
    </header>
    <!-- Fin Header -->

    <!-- Products -->
    <div class="container g-pt-150 g-pb-80">
        <div class="row">
            <!-- Content -->
            <div class="col-md-9 order-md-2">

                <div class="row g-mb-30">
                    <!-- Article Image -->
                    <div class="col-md-5">
                        <img class="img-fluid g-mb-20 g-mb-0--md" src="assets/global/img/socios/logos/logo-maindsteel-automotive.png" alt="Maindsteel Automotive">
                    </div>
                    <!-- End Article Image -->

                    <!-- Article Content -->
                    <div class="col-md-7 align-self-center">

                        <!-- Article Title -->
                        <h3 class="h3 g-font-weight-300 g-mb-15">

                            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover text-uppercase" href="#!">Maindsteel Automotive</a>

                        </h3>
                        <!-- End Article Title -->

                        <!-- Article Text -->
                        <div class="g-mb-30">
                            <p>Desde su fundación en 2006 Maindsteel Automotive se ha posicionado como líder en el diseño y manufactura de partes automotrices y comercialización de productos metálicos para diferentes aplicaciones.</p>
                            <a class="g-color-primary--hover g-color-primary--hover g-color-black--focus" href="#!">Leer Más</a>
                        </div>
                        <!-- End Article Text -->

                        <!-- Article Data Icons -->
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item g-mr-15">
                                <a class="u-icon-v3 u-icon-size--sm g-bg-facebook g-bg-gray-light-v5 g-bg-gray-light-v3--hover g-color-main rounded" href="#!">
                                    <i class="fa fa-map-marker"></i>
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-15">
                                <a class="u-icon-v3 u-icon-size--sm g-bg-facebook g-bg-gray-light-v5 g-bg-gray-light-v3--hover g-color-main rounded" href="#!">
                                    <i class="fa fa-phone"></i>
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-15">
                                <a class="u-icon-v3 u-icon-size--sm g-bg-facebook g-bg-gray-light-v5 g-bg-gray-light-v3--hover g-color-main rounded" href="#!">
                                    <i class="fa fa-globe"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Article Data Icons -->
                    </div>
                    <!-- End Article Content -->
                </div>

                <div class="row g-mb-30">
                    <!-- Article Image -->
                    <div class="col-md-5">
                        <img class="img-fluid g-mb-20 g-mb-0--md" src="assets/global/img/socios/logos/logo-a-and-i.png" alt="Maindsteel Automotive">
                    </div>
                    <!-- End Article Image -->

                    <!-- Article Content -->
                    <div class="col-md-7 align-self-center">

                        <!-- Article Title -->
                        <h3 class="h3 g-font-weight-300 g-mb-15">

                            <a class="u-link-v5 g-color-gray-dark-v2 g-color-primary--hover text-uppercase" href="#!">Metal Stamping</a>

                        </h3>
                        <!-- End Article Title -->

                        <!-- Article Text -->
                        <div class="g-mb-30">
                            <p>A & I Metal Stamping se está enfocando en traer los mejores servicios a la industria automotriz para lograr los requisitos por medio del estricto control del proceso de fabricación basado en el Estándar de Gestión de Calidad Automotriz IATF 16949 y la comprensión completa de los requisitos del cliente.</p>
                            <a class="g-color-primary--hover g-color-primary--hover g-color-black--focus" href="#!">Leer Más</a>
                        </div>
                        <!-- End Article Text -->

                        <!-- Article Data Icons -->
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item g-mr-15">
                                <a class="u-icon-v3 u-icon-size--sm g-bg-facebook g-bg-gray-light-v5 g-bg-gray-light-v3--hover g-color-main rounded" href="#!">
                                    <i class="fa fa-map-marker"></i>
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-15">
                                <a class="u-icon-v3 u-icon-size--sm g-bg-facebook g-bg-gray-light-v5 g-bg-gray-light-v3--hover g-color-main rounded" href="#!">
                                    <i class="fa fa-phone"></i>
                                </a>
                            </li>
                            <li class="list-inline-item g-mr-15">
                                <a class="u-icon-v3 u-icon-size--sm g-bg-facebook g-bg-gray-light-v5 g-bg-gray-light-v3--hover g-color-main rounded" href="#!">
                                    <i class="fa fa-globe"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Article Data Icons -->
                    </div>
                    <!-- End Article Content -->
                </div>

            </div>
            <!-- End Content -->

            <!-- Filters -->
            <div class="col-md-3 order-md-1 g-brd-right--lg g-brd-gray-light-v4 g-pt-40">
                <div class="g-pr-15--lg g-pt-60">
                    <!-- Categories -->
                    <div class="g-mb-30">
                        <h3 class="h5 mb-3">Sectores</h3>

                        <ul class="list-unstyled">
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Automotriz
                                    <span class="float-right g-font-size-12">2</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Agroindustrial
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Automatización
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Consultoría
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Industrial
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Manufactura
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Médico
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">Minería
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                            <li class="my-3">
                                <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">TIC's
                                    <span class="float-right g-font-size-12">0</span></a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Categories -->

                    <hr>

                    <!-- Brand -->
                    <div class="g-mb-30">
                        <h3 class="h5 mb-3">Certificaciones</h3>

                        <ul class="list-unstyled">
                            <li class="my-2">
                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked>
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </span>
                                        ISO 9001
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </span>
                                    ISO/TS 16949-IAFT 16949
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </span>
                                    ISO/AS 9100
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                    </span>
                                    ISO 14001
                                </label>
                            </li>
                        </ul>
                    </div>
                    <!-- End Brand -->

                    <hr>

                    <!-- Brand -->
                    <div class="g-mb-30">
                        <h3 class="h5 mb-3">País</h3>

                        <select id="select-paises" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">
                            <option value="Pais-1">México</option>
                            <option value="Pais-2">Estados Unidos</option>
                        </select>
                    </div>
                    <!-- End Brand -->

                    <hr>

                    <!-- Brand -->
                    <div class="g-mb-30">
                        <h3 class="h5 mb-3">Estado</h3>

                        <select id="select-estados" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">
                            <option value="Pais-1">Aguascalientes</option>
                            <option value="Pais-2">Zacatecas</option>
                        </select>
                    </div>
                    <!-- End Brand -->

                    <hr>

                    <!-- Brand -->
                    <div class="g-mb-30">
                        <h3 class="h5 mb-3">Municipio</h3>

                        <select id="select-municipio" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">
                            <option value="Pais-1">Aguascalientes</option>
                            <option value="Pais-2">Rincón de Romos</option>
                        </select>
                    </div>
                    <!-- End Brand -->

                    <hr>

                    <button class="btn btn-block u-btn-black g-font-size-12 text-uppercase g-py-12 g-px-25" type="button">Filtrar</button>
                </div>
            </div>
            <!-- End Filters -->
        </div>
    </div>
    <!-- End Products -->

    <!-- Footer -->
    <footer>
        <div class='g-bg-white'>
            <div class="container g-pt-70 g-pb-40">
                <div class="row">
                    <div class="col-6 col-md-3 g-mb-30">
                        <h3 class="h6 g-color-black-opacity-0_7 text-uppercase"><b>Sectores</b></h3>

                        <!-- Links -->
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Automotriz</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Agroindustrial</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Automatización</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Consultoría</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Industrial</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                        </ul>
                        <!-- End Links -->
                    </div>

                    <div class="col-6 col-md-3 g-mb-30">
                        <h3 class="h6 g-color-black-opacity-0_7 text-uppercase g-mb-25"><b></b></h3>

                        <!-- Links -->
                        <ul class="list-unstyled mb-0">

                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Manufactura</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Médico</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>Minería</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>
                            <li>
                                <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                    <span>TIC's</span>
                                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                </a>
                            </li>

                        </ul>
                        <!-- End Links -->
                    </div>

                    <div class="col-6 col-md-3 g-mb-30">
                        <h2 class="h5 g-color-black-opacity-0_7 mb-4"><b>Contacto</b></h2>
                        <!-- Links -->
                        <ul class="list-unstyled g-color-gray-dark-v5 g-font-size-13">
                            <li class="media my-3">
                                <i class="d-flex mt-1 mr-3 fa fa-map-marker"></i>
                                <div class="media-body">
                                    <a href="https://goo.gl/maps/RcRr5YQJbHBP3BFq8" class="g-color-black-opacity-0_7 g-color-primary--hover" target="_blank">Av. Jorge Reynoso 1703-15, Interior A
                                        Desarrollo Especial Galerías
                                        C.P.20120</a>
                                </div>
                            </li>
                            <li class="media my-3">
                                <i class="d-flex mt-1 mr-3 fa fa-phone"></i>
                                <div class="media-body">
                                    <a href="tel:449-549-4456" class="g-color-black g-color-primary--hover">+52-1-449-549-4456</a>
                                </div>
                            </li>
                        </ul>
                        <!-- End Links -->
                    </div>

                    <div class="col-6 col-md-3 g-mb-30">
                        <a href='javascript:void(0);'> <img src='assets/global/img/logo/logo-web-segura.png' class='g-height-105 g-mb-10'> </a>
                        <br>
                        <a href='https://grupomaen.mx/' target='_blank'> <img src='assets/global/img/logo/logo-grupo-maen.png' class='g-height-40  g-mb-10'> </a>
                    </div>
                    <div class="col-md-12 text-center g-mb-20">
                        <a href="https://maindsoft.net/" target="_blank"><img src="assets/global/img/logo/logo-maindsoft.png" class="g-height-35"></a>
                    </div>
                </div>
            </div>
            <!-- Copyright -->

        </div>
    </footer>
    <!-- End Footer -->

</main>
</body>

<!-- JS Global Compulsory -->
<!-- jquery -->
<script src="assets/global/js/jquery/jquery-3-3-1.min.js"></script>
<script src="assets/global/js/jquery/jquery-migrate.min.js"></script>
<script src="assets/global/js/jquery/jquery-ui.min.js"></script>
<!-- popper -->
<script src="assets/global/js/popper/popper.min.js"></script>
<!-- bootstrap -->
<script src="assets/global/js/bootstrap/bootstrap.min.js"></script>
<!-- Apear -->
<script src="assets/global/js/appear/appear.min.js"></script>
<script src="assets/global/js/jquery.filer/js/jquery.filer.min.js"></script>
<!-- megamenu -->
<script src="assets/global/js/megamenu/hs.megamenu.js"></script>
<!-- Components -->
<script src="assets/global/js/hs.core.js"></script>
<!-- Header -->
<script src="assets/global/js/header/hs.header.js"></script>
<script src="assets/global/js/navigation/hs.navigation.js"></script>
<script src="assets/global/js/hamburgers/hs.hamburgers.js"></script>
<script src="assets/global/js/tabs/hs.tabs.js"></script>
<!-- Map -->
<script src="assets/global/js/map/gmaps.min.js"></script>
<script src="assets/global/js/map/hs.map.js"></script>
<!-- File -->
<script src="assets/global/js/file/hs.focus-state.js"></script>
<script src="assets/global/js/file/hs.file-attachement.js"></script>
<script src="assets/global/js/file/hs.file-attachments.js"></script>
<!-- Custom  -->
<script src="assets/global/js/custom.js"></script>
<script src="assets/global/js/reglas.js"></script>

<script>

    search_bar_autocomplete();

    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSNavigation component
        $.HSCore.components.HSNavigation.init( $('.js-navigation') );

        // initialization of tabs
        //$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of forms
        $.HSCore.helpers.HSFileAttachments.init();
        $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
        $.HSCore.helpers.HSFocusState.init();
    });

</script>

<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBmQYhY3TRKgiY-qbwWuIemSzdVLlVigVI&callback=initMap" async defer></script>

</html>