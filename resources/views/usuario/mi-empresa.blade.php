<!--
    * Nombre: mi-empresa.php
    * Creado por: Luis Cristerna (MAINDSOFT SISTEMAS INTEGRALES)
    * Fecha: 02/09/2019
-->

<?php $perfil_usuario = session()->get('perfilUsuario');?>

        <!DOCTYPE html>
<html lang="es" class="font-primary">

<head>


    <title>Mi Cuenta | {{print_r($perfil_usuario['usuario']->NOMBRE, true)}}</title>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Tag para que que los rastreadores web de la mayoría de los motores de búsqueda indexen esta página-->
    <meta name="robots" content="noindex">
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="keywords" content="Grupo,MAEN,Aguascalientes">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta name="description"
          content="NA">
    <!--Meta imagen (Imagen que se muestra al compartir la pagina)-->
    <meta name="image" content="NA">
    <!--Color de la pestana de la pagina (Utilizada en chrome para moviles, por ejemplo)-->
    <meta name="theme-color" content="#ED1C24">
    <!-- Meta Tags (Facebook, Pinterest & Google+) -->
    <!--Titulo de la pagina-->
    <meta property="og:title" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta property="og:description"
          content="NA">
    <!--URL de la pagina-->
    <meta property="og:url" content="https://grupomaen.mx/">
    <!--Nombre de la pagina-->
    <meta property="og:site_name" content="Grupo MAEN">
    <!--De que pais es la pagina, mas no el servidor-->
    <meta property="og:locale" content="es_MX">
    <!--Tipo de la pagina: sitio web-->
    <meta property="og:type" content="website"/>
    <!--Imagen al compartir-->
    <meta property="og:image" content="NA"/>
    <!--ID FB-->
    <meta property="fb:app_id" content="NA"/>
    <!-- Meta Tags Google -->
    <!--Titulo de la pagina-->
    <meta itemprop="name" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta itemprop="description"
          content="NA">

    <!-- Fuentes necesarias para la pagina -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600">
    <!-- Fin de fuentes -->

    <!-- Fav-->
    <link rel="shortcut icon" href="/assets/global/img/favicon/fav.ico">

    <!-- CSS -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap/bootstrap.min.css">

    <!-- jquery -->
    <link rel="stylesheet" href="/assets/global/css/jquery/jquery-ui.css">

    <!-- Icons -->
    <link rel="stylesheet" href="/assets/global/css/icons/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/global/css/icons/icon-hs/style.css">

    <!-- header -->
    <link rel="stylesheet" href="/assets/global/css/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/assets/global/css/megamenu/hs.megamenu.css">

    <!-- Animate -->
    <link rel="stylesheet" href="/assets/global/css/animate/animate.css">

    <!-- Unify -->
    <link rel="stylesheet" href="/assets/global/css/unify/unify.min.css">

    <!-- custom -->
    <link rel="stylesheet" href="/assets/global/css/custom.css">

    <!-- Alertify  -->
    <link rel="stylesheet" href="/assets/global/css/alertify/alertify.min.css">
    <link rel="stylesheet" href="/assets/global/css/alertify/themes/default.rtl.min.css">

    <!-- End CSS -->

</head>

<body>
<main>


    @include('componentes.header')
    <?php $tamano_sector = 0?>
    <section class="g-py-170">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="container">
            <div class="row">
                <!-- Barra De Perfil -->
                <div class="col-lg-3 g-mb-50 g-mb-0--lg">
                    <!-- Foto de Perfil -->
                    <div class="u-block-hover">
                        <figure>
                            <img class="img-fluid w-100 u-block-hover__main--zoom-v1"
                                 src="/assets/global/img/empresas/empresas-placeholder.jpg"
                                 alt="Foto Perfil">
                        </figure>
                        <!-- Ajustes de foto de perfil -->
                        <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                            <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                <!-- Iconos de ajustes de foto de perfil -->
                                <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                    <li class="list-inline-item align-middle g-mx-7">
                                        <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                            <i class="fa fa-camera u-line-icon-pro"
                                               alt="Cambiar Foto de Perfil"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Fin Iconos de Ajustes de foto de perfil -->
                            </div>
                        </figcaption>
                        <!-- Fin Ajustes de foto de perfil-->
                        <!-- Información del Usuario -->
                        <span class="g-pos-abs g-top-20 g-left-0">
                                    <a class="btn btn-sm u-btn-primary rounded-0"
                                       href="#!"> {{$perfil_usuario['usuario']->NOMBRE}} </a>
                                    <small class="d-block g-bg-black g-color-white g-pa-5">Admin.</small>
                                </span>
                        <!-- Fin Información del Usuario -->
                    </div>
                    <!-- Fin Foto de Perfil -->
                    <!-- Barra de Navegación  -->
                    <div class="list-group list-group-border-0 g-mb-40">
                        <!-- Info General de la empresa -->
                        <a href="#!" class="list-group-item justify-content-between active">
                            <span><i class="fa fa-industry g-pos-rel g-top-1 g-mr-8"></i> Información general de la empresa</span>
                            <!-- Notificaciones: Por el momento documentado
                                <span class="u-label g-font-size-11 g-bg-white g-color-main g-rounded-20 g-px-10">2</span>
                            -->
                        </a>
                        <!-- Fin Info General de la empresa -->
                        <!-- Cotizaciones -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-dollar g-pos-rel g-top-1 g-mr-8"></i> Solicitud de Cotizaciones</span>
                        </a>
                        <!-- Fin Cotizaciones -->
                        <!-- Portal de empleo -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-user g-pos-rel g-top-1 g-mr-8"></i> Portal de empleo</span>
                        </a>
                        <!-- Fin Barra de Navegación  -->
                        <!-- Lista Blanca -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-check g-pos-rel g-top-1 g-mr-8"></i> Lista Blanca</span>
                        </a>
                        <!-- Fin Lista Blanca -->
                        <!-- Lista Negra -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-close g-pos-rel g-top-1 g-mr-8"></i> Lista Negra</span>
                        </a>
                        <!-- Fin Lista Negra -->
                        <!-- Outsourcing -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-user g-pos-rel g-top-1 g-mr-8"></i> Outsourcing</span>
                        </a>
                        <!-- Fin Outsourcing -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-briefcase g-pos-rel g-top-1 g-mr-8"></i> Fletes</span>
                        </a>
                        <!-- Encuestas -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-sticky-note g-pos-rel g-top-1 g-mr-8"></i> Encuestas</span>
                        </a>
                        <!-- Fin Encuestas -->
                        <!-- Cerrar Sesion -->
                        <a href="{{ route('logout') }}"
                           class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-power-off g-pos-rel g-top-1 g-mr-8"></i> Cerrar Sesión</span>
                        </a>
                        <!-- Fin Cerrar Sesion -->
                    </div>
                    <!-- Fin Navegación -->
                </div>


                <!-- Contenido De Perfil de Empresa -->
                    <div class="col-lg-9">
                        <div id="loading" style="display: none">
                           <img class="loader">
                        </div>

                        <div id="enterprise">
                            @include('componentes.empresa')

                        </div>

                        <!-- Guardar Cambios -->
                        <div class="col-md-12">
                            <button class="btn btn-md u-btn-inset u-btn-black g-mr-10 g-mb-15 form-control g-color-white text-uppercase g-bg-black--hover g-bg-black--focus g-color-white--hover g-color-white--active"
                                    type="button" onclick="updateEnterprise()">Guardar Cambios
                            </button>
                        </div>
                        <!-- Fin Guardar Cambios -->
                    </div>
                    <!-- Fin Contenido De Perfil de Empresa -->

            </div>
        </div>
    </section>
</main>

</body>

<!-- JS Global Compulsory -->
<!-- jquery -->
<script src="/assets/global/js/jquery/jquery-3-3-1.min.js"></script>
<script src="/assets/global/js/jquery/jquery-migrate.min.js"></script>
<script src="/assets/global/js/jquery/jquery-ui.min.js"></script>
<!-- popper -->
<script src="/assets/global/js/popper/popper.min.js"></script>
<!-- bootstrap -->
<script src="/assets/global/js/bootstrap/bootstrap.min.js"></script>
<!-- Apear -->
<script src="/assets/global/js/appear/appear.min.js"></script>
<script src="/assets/global/js/jquery.filer/js/jquery.filer.min.js"></script>
<!-- megamenu -->
<script src="/assets/global/js/megamenu/hs.megamenu.js"></script>
<!-- Components -->
<script src="/assets/global/js/hs.core.js"></script>
<!-- Header -->
<script src="/assets/global/js/header/hs.header.js"></script>
<script src="/assets/global/js/navigation/hs.navigation.js"></script>
<script src="/assets/global/js/hamburgers/hs.hamburgers.js"></script>
<script src="/assets/global/js/tabs/hs.tabs.js"></script>
<!-- Map -->
<script src="/assets/global/js/map/gmaps.min.js"></script>
<script src="/assets/global/js/map/hs.map.js"></script>
<!-- File -->
<script src="/assets/global/js/file/hs.focus-state.js"></script>
<script src="/assets/global/js/file/hs.file-attachement.js"></script>
<script src="/assets/global/js/file/hs.file-attachments.js"></script>
<!-- Alertify  -->
<script src="/assets/global/js/alertify/alertify.min.js"></script>
<!-- Custom  -->
<script src="/assets/global/js/custom.js"></script>
<script src="/assets/global/js/reglas.js"></script>

<script>

    search_bar_autocomplete();

    function getSector() {
        var size = <?= count($sectores) ?>;
        var sector = '';
        var resultado = "";

        for (i = 0; i < size; i++){
            if ($('#sector-' + i).is(':checked') === true){
                resultado = resultado + ($('#sector-span-' + i).text()) + ",";
            }
        }

        //Se retorna el substring de resultado ya que se quita la ultima coma de la cadena
        return resultado.slice(0,-1);
    }

    function getCertificate() {
        var size = <?= count($certificaciones) ?>;
        var sector = '';
        var resultado = "";

        for (i = 0; i < size; i++){
            if ($('#certificacion-' + i).is(':checked') === true){
                resultado = resultado + ($('#certificacion-span-' + i).text()) + ",";
            }
        }

        //Se retorna el substring de resultado ya que se quita la ultima coma de la cadena
        return resultado.slice(0,-1);
    }

    function href_div_scroll(id_div) {
        $("html, body").animate({scrollTop: $('#' + id_div).offset().top }, 1000);
    }

    function getId() {

        id = '<?=$perfil_usuario['usuario']->RFC_EMPRESA?>';
        return id;
    }

    function updateEnterprise() {

        $(document).ajaxStart(function() {
            $("#loading").show();
        }).ajaxStop(function() {
            $("#loading").hide();
        });

        //Municipio
        selectElementMunicipality = document.querySelector('#select-municipio');

        var razon_social = $('#razon_social').val();
        var nombre_comercial = $('#nombre_comercial').val();
        var telefono_empresa = $('#telefono_empresa').val();
        var email_empresa = $('#email_empresa').val();
        var horario_empresa = $('#horario_empresa').val();
        var calle_domicilio = $('#calle_domicilio').val();
        var num_int_ext = $('#num_int_ext').val();
        var colonia = $('#colonia').val();
        var codigo_postal = $('#codigo_postal').val();
        var link_google_maps = $('#link_google_maps').val();
        var descripcion_empresa = $('#descripcion_empresa').val();
        var premios_obtenidos = $('#premios_obtenidos').val();
        var num_empleados = $('#num_empleados').val();
        var crecimiento_anual = $('#crecimiento_anual').val();
        var ventas_anuales = $('#ventas_anuales').val();
        var patentes_registradas = $('#patentes_registradas').val();
        var facebook_empresa = $('#facebook_empresa').val();
        var youtube_empresa = $('#youtube_empresa').val();
        var linkedin_empresa = $('#linkedin_empresa').val();
        var twitter_empresa = $('#twitter_empresa').val();
        var instagram_empresa = $('#instagram_empresa').val();
        var id = getId();
        var sectores = getSector();
        var certificaciones = getCertificate();
        var municipio = selectElementMunicipality.value;

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: "<?= Route('actualizar-empresa')?>",
            dataType: 'json',
            data: {
                razon_social: razon_social,
                id: id,
                nombre_comercial: nombre_comercial,
                telefono_empresa: telefono_empresa,
                email_empresa: email_empresa,
                horario_empresa: horario_empresa,
                sectores: sectores,
                municipio: municipio,
                calle_domicilio: calle_domicilio,
                num_int_ext: num_int_ext,
                colonia: colonia,
                codigo_postal: codigo_postal,
                link_google_maps: link_google_maps,
                descripcion_empresa: descripcion_empresa,
                premios_obtenidos: premios_obtenidos,
                certificaciones: certificaciones,
                num_empleados: num_empleados,
                crecimiento_anual: crecimiento_anual,
                ventas_anuales: ventas_anuales,
                patentes_registradas: patentes_registradas,
                facebook_empresa: facebook_empresa,
                youtube_empresa: youtube_empresa,
                linkedin_empresa: linkedin_empresa,
                twitter_empresa: twitter_empresa,
                instagram_empresa: instagram_empresa,

            },
            error: function (msg) {

                if (msg.responseJSON.errors.razon_social){
                    $("#error-razon-social").show().text(msg.responseJSON.errors.razon_social[0]);
                }else {
                    $("#error-razon-social").hide();
                }
                if (msg.responseJSON.errors.nombre_comercial){
                    $("#error-nombre-comercial").show().text(msg.responseJSON.errors.nombre_comercial[0]);
                }else {
                    $("#error-nombre-comercial").hide();
                }
                if (msg.responseJSON.errors.telefono_empresa){
                    $("#error-telefono-empresa").show().text(msg.responseJSON.errors.telefono_empresa[0]);
                }else {
                    $("#error-telefono-empresa").hide();
                }
                if (msg.responseJSON.errors.email_empresa){
                    $("#error-email-empresa").show().text(msg.responseJSON.errors.email_empresa[0]);
                }else {
                    $("#error-email-empresa").hide();
                }
                if (msg.responseJSON.errors.horario_empresa){
                    $("#error-horario-empresa").show().text(msg.responseJSON.errors.horario_empresa[0]);
                }else {
                    $("#error-horario-empresa").hide();
                }
                if (msg.responseJSON.errors.calle_domicilio){
                    $("#error-calle-domicilio").show().text(msg.responseJSON.errors.calle_domicilio[0]);
                }else {
                    $("#error-calle-domicilio").hide();
                }
                if (msg.responseJSON.errors.num_int_ext){
                    $("#error-num-int-ext").show().text(msg.responseJSON.errors.num_int_ext[0]);
                }else {
                    $("#error-num-int-ext").hide();
                }
                if (msg.responseJSON.errors.colonia){
                    $("#error-colonia").show().text(msg.responseJSON.errors.colonia[0]);
                }else {
                    $("#error-colonia").hide();
                }
                if (msg.responseJSON.errors.codigo_postal){
                    $("#error-codigo-postal").show().text(msg.responseJSON.errors.codigo_postal[0]);
                }else {
                    $("#error-codigo-postal").hide();
                }
                if (msg.responseJSON.errors.link_google_maps){
                    $("#error-link-google-maps").show().text(msg.responseJSON.errors.link_google_maps[0]);
                }else {
                    $("#error-link-google-maps").hide();
                }
                if (msg.responseJSON.errors.descripcion_empresa){
                    $("#error-descripcion-empresa").show().text(msg.responseJSON.errors.descripcion_empresa[0]);
                }else {
                    $("#error-descripcion-empresa").hide();
                }
                if (msg.responseJSON.errors.premios_obtenidos){
                    $("#error-premios-obtenidos").show().text(msg.responseJSON.errors.premios_obtenidos[0]);
                }else {
                    $("#error-premios-obtenidos").hide();
                }
                if (msg.responseJSON.errors.num_empleados){
                    $("#error-num-empleados").show().text(msg.responseJSON.errors.num_empleados[0]);
                }else {
                    $("#error-num-empleados").hide();
                }
                if (msg.responseJSON.errors.crecimiento_anual){
                    $("#error-crecimiento-anual").show().text(msg.responseJSON.errors.crecimiento_anual[0]);
                }else {
                    $("#error-crecimiento-anual").hide();
                }
                if (msg.responseJSON.errors.ventas_anuales){
                    $("#error-ventas-anuales").show().text(msg.responseJSON.errors.ventas_anuales[0]);
                }else {
                    $("#error-ventas-anuales").hide();
                }
                if (msg.responseJSON.errors.patentes_registradas){
                    $("#error-patentes-registradas").show().text(msg.responseJSON.errors.patentes_registradas[0]);
                }else {
                    $("#error-patentes-registradas").hide();
                }
                if (msg.responseJSON.errors.facebook_empresa){
                    $("#error-facebook-empresa").show().text(msg.responseJSON.errors.facebook_empresa[0]);
                }else {
                    $("#error-facebook-empresa").hide();
                }


            },
            success: function (msg) {
                $("#enterprise").html(msg.html);
                alertify.set('notifier','position', 'top-center');
                alertify.success('Se guardaron todos los cambios correctamente.');
            }

        });
    }

    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSNavigation component
        $.HSCore.components.HSNavigation.init($('.js-navigation'));

        // initialization of tabs
        //$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of forms
        $.HSCore.helpers.HSFileAttachments.init();
        $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
        $.HSCore.helpers.HSFocusState.init();
    });

</script>

<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBmQYhY3TRKgiY-qbwWuIemSzdVLlVigVI&callback=initMap" async
        defer></script>

</html>
