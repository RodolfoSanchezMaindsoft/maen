<!-- Footer -->
<footer>
    <div class='g-bg-white'>
        <div class="container g-pt-70 g-pb-40">
            <div class="row">
                <div class="col-6 col-md-3 g-mb-30">
                    <h3 class="h6 g-color-black-opacity-0_7 text-uppercase"><b>Sectores</b></h3>

                    <!-- Links -->
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Automotriz</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Agroindustrial</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Automatización</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Consultoría</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Industrial</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                    </ul>
                    <!-- End Links -->
                </div>

                <div class="col-6 col-md-3 g-mb-30">
                    <h3 class="h6 g-color-black-opacity-0_7 text-uppercase g-mb-25"><b></b></h3>

                    <!-- Links -->
                    <ul class="list-unstyled mb-0">

                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Manufactura</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Médico</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>Minería</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-black-opacity-0_7 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="javascript:void(0);">
                                <span>TIC's</span>
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>

                    </ul>
                    <!-- End Links -->
                </div>

                <div class="col-6 col-md-3 g-mb-30">
                    <h2 class="h5 g-color-black-opacity-0_7 mb-4"><b>Contacto</b></h2>
                    <!-- Links -->
                    <ul class="list-unstyled g-color-gray-dark-v5 g-font-size-13">
                        <li class="media my-3">
                            <i class="d-flex mt-1 mr-3 fa fa-map-marker"></i>
                            <div class="media-body">
                                <a href="https://goo.gl/maps/RcRr5YQJbHBP3BFq8" class="g-color-black-opacity-0_7 g-color-primary--hover" target="_blank">Av. Jorge Reynoso 1703-15, Interior A
                                    Desarrollo Especial Galerías
                                    C.P.20120</a>
                            </div>
                        </li>
                        <li class="media my-3">
                            <i class="d-flex mt-1 mr-3 fa fa-phone"></i>
                            <div class="media-body">
                                <a href="tel:449-549-4456" class="g-color-black g-color-primary--hover">+52-1-449-549-4456</a>
                            </div>
                        </li>
                    </ul>
                    <!-- End Links -->
                </div>

                <div class="col-6 col-md-3 g-mb-30">
                    <a href='javascript:void(0);'> <img src='/assets/global/img/logo/logo-web-segura.png' class='g-height-105 g-mb-10'> </a>
                    <br>
                    <a href='https://grupomaen.mx/' target='_blank'> <img src='/assets/global/img/logo/logo-grupo-maen.png' class='g-height-40  g-mb-10'> </a>
                </div>
                <div class="col-md-12 text-center g-mb-20">
                    <a href="https://maindsoft.net/" target="_blank"><img src="/assets/global/img/logo/logo-maindsoft.png" class="g-height-35"></a>
                </div>
            </div>
        </div>
        <!-- Copyright -->

    </div>
</footer>
<!-- End Footer -->