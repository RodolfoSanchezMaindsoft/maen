<!-- Header -->
<header id="js-header" class="u-header u-header--sticky-top u-header--toggle-section u-header--change-appearance " data-header-fix-moment="200" data-header-fix-effect="slide">
    <!-- Header Parte Arriba -->
    <div class="text-center text-lg-left u-header__section u-header__section--hidden u-header__section--light g-bg-white g-brd-bottom g-brd-gray-light-v4 g-py-5 g-pt-10 g-hidden-md-down">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-4 g-hidden-sm-down text-center">
                    <!-- Logo -->
                    <a href="javascript:;" class="js-go-to navbar-brand u-header__logo" data-type="static">
                        <img class="u-header__logo-img u-header__logo-img--main g-width-140" src="/assets/global/img/logo/logo-maen.png" alt="Logo | Grupo Maen">
                    </a>
                    <!-- Fin Logo -->
                </div>
                <!-- La clase auto-widget junto con el input inp-sector-auto-maen son los necesarios para tener el autocompletado-->
                <div class="col-xl-6 col-lg-5 g-hidden-md-down auto-widget g-mt-10">
                    <!-- Form -->
                    <form class="input-group rounded">
                        <!-- input inp-sector-auto-maen -->
                        <input id="inp-sector-auto-maen" class="form-control g-brd-secondary-light-v2 g-brd-primary--focus g-color-secondary-dark-v1 g-placeholder-secondary-dark-v1 g-bg-white g-font-weight-400 g-font-size-13 g-px-20 g-py-12" type="text" placeholder="Buscar producto, proceso, servicio o palabra clave">
                        <span class="input-group-append g-brd-none g-py-0 g-pr-0">
                                <button onclick="change_page();" class="btn u-btn-white g-color-primary--hover g-bg-secondary g-font-weight-600 g-font-size-13 text-uppercase g-py-12 g-px-20" type="button" >
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        <!-- Fin input inp-sector-auto-maen -->
                    </form>
                    <!-- Fin Form -->
                </div>
                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 text-center g-hidden-xs-down g-mt-10">
                    <div class="g-pa-10">
                        <li class="list-inline">
                            <a class="dropdown-toggle no-text-underline g-color-gray-dark-v3" data-toggle="dropdown" href="javascript:;">
                                <span class="fa fa-globe g-color-primary"></span>
                                Idioma
                            </a>
                            <!-- Con el evento onclick mandamos llamar la funcion doGTranslate del script dado por gtranslate y el cual
                                     tenemos en el archivo custom.js. Ademas debe de llevar como parametro el lenguaje al que queremos cambiar
                                     al momento de dar click-->
                            <ul class="dropdown-langs dropdown-menu">
                                <li>
                                    <a class="g-color-gray-dark-v3" tabindex="-1" href="javascript:;" onclick="doGTranslate('es|es');">
                                        <img id="leng-esp" src="/assets/global/img/banderas/mx.ico" width="16" height="11" alt="Español"/>
                                        Español
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a class="g-color-gray-dark-v3" tabindex="-1" href="javascript:;" onclick="doGTranslate('es|en');">
                                        <img id="leng-ing" src="/assets/global/img/banderas/us.ico" width="16"
                                             height="11" alt="English"/>
                                        Inglés
                                    </a>
                                </li>
                            </ul>
                            <div id="google_translate_element2"></div>
                            <!-- Estilo necesario para el traductor -->
                            <style type="text/css">
                                a.gflag{vertical-align:middle;font-size:24px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/24.png)}a.gflag img{border:0}a.gflag:hover{background-image:url(//gtranslate.net/flags/24a.png)}#goog-gt-tt{display:none!important}.goog-te-banner-frame{display:none!important}.goog-te-menu-value:hover{text-decoration:none!important}body{top:0!important}#google_translate_element2{display:none!important}
                            </style>
                            <!-- Fin Estilo necesario para el traductor -->
                            <!-- Scripts necesario para el traductor -->
                            <script type="text/javascript">function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'es',autoDisplay: false}, 'google_translate_element2');}</script>
                            <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>
                            <!-- Fin Scripts necesario para el traductor -->
                        </li>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 text-center g-mt-10">
                    <div class="g-pa-10">
                        <a href="javascript:;" class="g-color-gray-dark-v5 g-color-primary--hover"><span>únete ahora</span></a>
                        <span>/</span>
                        <a href="javascript:;" class="g-color-gray-dark-v5 g-color-primary--hover g-brd-black"><span> Inicia sesión</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Header Parte Arriba -->

    <!-- Header Parte Abajo -->
    <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10" data-header-fix-moment-exclude="g-bg-white g-py-10" data-header-fix-moment-classes="g-bg-white-opacity-0_7 u-shadow-v18 g-py-0">
        <nav class="js-mega-menu navbar navbar-expand">
            <!-- Boton Responsivo Móvil -->
            <button class="col-2 navbar-toggler navbar-toggler-left btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-10
		                    g-right-12" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                            <span class="hamburger hamburger--slider">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </span>
            </button>
            <!-- Fin Boton Responsivo Móvil -->
            <!-- Logo Móvil -->
            <div class="navbar-brand col-10 g-hidden-lg-up auto-widget">
                <!-- Form -->
                <form class="input-group rounded">
                    <a id='img-logo-header' href="#!" class="navbar-brand g-mr-35">
                        <img src="/assets/global/img/logo/logo-maen-movil.png" alt="Logo Maen" height='26'>
                    </a>
                    <!-- input inp-sector-auto-maen -->
                    <input id="inp-sector-auto-maen-movil" onfocus='search_bar_mobile_focus()' onblur='search_bar_mobile_onblur()' class="form-control g-brd-secondary-light-v2 g-brd-primary--focus g-color-secondary-dark-v1 g-placeholder-secondary-dark-v1 g-bg-white g-font-weight-400 g-font-size-13 g-px-20 g-py-12" type="text" placeholder="Buscar...">
                    <button onclick='obtain_text_search_bar("Mobile")' id='btn-buscar-header' class="btn u-btn-white g-color-primary--hover g-bg-secondary g-font-weight-600 g-font-size-13 text-uppercase g-py-12 g-px-20" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
                <!-- Fin Form -->
            </div>
            <!-- Fin Logo Móvil -->

            <!-- Navegación -->
            <div class="js-navigation navbar-collapse align-items-center flex-sm-row u-main-nav--push-hidden g-pt-10 g-pt-5--lg g-mr-40--sm"
                 id="navBar" data-navigation-breakpoint="lg" data-navigation-position="left" data-navigation-init-classes="g-transition-0_5"
                 data-navigation-init-body-classes="g-transition-0_5" data-navigation-overlay-classes="g-bg-black-opacity-0_8 g-transition-0_5">
                <div class="u-main-nav__list-wrapper mr-auto g-overflow-y-auto-lg">
                    <ul class="navbar-nav text-uppercase g-font-weight-600 mr-auto">
                        <li class="nav-item g-mx-20--lg">
                            <a href="javascript:void(0);" class="nav-link px-0">INICIO</a>
                        </li>
                        <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                            <a href="#!" class="nav-link g-px-0" id="nav-link-1" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-1">Nosotros</a>
                            <!-- Submenu -->
                            <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-1" aria-labelledby="nav-link-1">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Grupo Maen</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Comisiones</a>
                                </li>
                            </ul>
                            <!-- Fin Submenu -->
                        </li>

                        <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                            <a href="#!" class="nav-link g-px-0" id="nav-link-2" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-2">Productos</a>
                            <!-- Submenu -->
                            <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-2">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Exhibidores</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Mostradores</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Alarmas</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Extintores</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Jaladeras</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Cofres para Tractos</a>
                                </li>
                            </ul>
                            <!-- Fin Submenu -->
                        </li>

                        <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                            <a href="#!" class="nav-link g-px-0" id="nav-link-3" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-3">Procesos</a>
                            <!-- Submenu -->
                            <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-3">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Estampado CNC</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Doblado de Tubo</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Corte Láser</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Acabados Metálicos</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Formado de Alambre</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Inyección de Plástico</a>
                                </li>
                            </ul>
                            <!-- Fin Submenu -->
                        </li>

                        <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                            <a href="#!" class="nav-link g-px-0" id="nav-link-4" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-4">Servicios</a>
                            <!-- Submenu -->
                            <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-4">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Instalación de Alarmas </a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Desarrollo de Apps.</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Curso para Técnicos</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Partes Estampadas</a>
                                </li>
                            </ul>
                            <!-- Fin Submenu -->
                        </li>

                        <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                            <a href="#!" class="nav-link g-px-0" id="nav-link-5" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-5">Empresas</a>
                            <!-- Submenu -->
                            <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-2" aria-labelledby="nav-link-5">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Maindsteel</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Maindsoft</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Metal Stamping</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">SOIN3</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">QMC</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" href="#!">Maindsteel Automotive</a>
                                </li>
                            </ul>
                            <!-- Fin Submenu -->
                        </li>

                        <li class="nav-item g-mx-20--lg">
                            <a href="javascript:void(0);" class="nav-link px-0">Blog</a>
                        </li>
                        <li class="nav-item g-mx-20--lg">
                            <a href="javascript:void(0);" class="nav-link px-0">Contacto</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Fin Navegación -->
        </nav>
    </div>
    <!-- Fin Header Parte Abajo -->
</header>
<!-- Fin Header -->