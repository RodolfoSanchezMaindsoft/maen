@if ($empresas)
    @foreach($empresas as $empresa)
        <!-- Información sobre la empresa -->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">
                    Información de la empresa</h3>
                <!-- Input Razón Social -->
                <div class="form-group g-mb-30 {{ $errors->has('razon_social') ? 'g-color-red': '' }}">
                    <label class="g-mb-10 g-color-black" for="razon_social">Razón social <span
                                class="g-color-red">*</span></label>
                    <div class="g-pos-rel">
                        <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                               value="{{$empresa->RAZON_SOCIAL}}"
                               name="razon_social"
                               id="razon_social"
                               type="text"
                               placeholder="Nombre de la empresa SA de CV">
                        {!! $errors->first('razon_social', '<br><span class="help-block">:message</span>') !!}
                        <div class="g-mt-5">
                            <span id="error-razon-social" style="display: none" class="g-color-red"></span>
                        </div>
                    </div>
                </div>
                <!-- Fin Input Razón Social -->
                <div class="row">
                    <!-- Input Nombre Comercial-->
                    <div class="col-md-6  {{ $errors->has('nombre_comercial') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="nombre_comercial">Nombre Comercial <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       value="{{$empresa->NOMBRE_COMERCIAL}}"
                                       type="text"
                                       name="nombre_comercial"
                                       id="nombre_comercial"
                                       placeholder="Nombre por el cual es reconocido la empresa en el mercado">
                                {!! $errors->first('nombre_comercial', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-nombre-comercial" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Nombre Comercial-->

                    <!-- Input RFC-->
                    <div class="col-md-6 {{ $errors->has('rfc') ? 'g-color-red': '' }} ">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="input-rfc">RFC <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input id="input-rfc" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       value="{{$empresa->RFC}}"
                                       type="text"
                                       name="rfc"
                                       id="rfc"
                                       placeholder="RFC"
                                       disabled>
                                {!! $errors->first('rfc', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-rfc" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input RFC-->

                    <!-- Input Teléfono de la empresa-->
                    <div class="col-md-6 {{ $errors->has('telefono_empresa') ? 'g-color-red': '' }} ">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="telefono_empresa">Teléfono de la empresa <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       value="{{$empresa->TELEFONO}}"
                                       type="number"
                                       name="telefono_empresa"
                                       id="telefono_empresa"
                                       placeholder="Ejemplo: 4491112233">
                                {!! $errors->first('telefono_empresa', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-telefono-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Teléfono de la empresa-->

                    <!-- Input Email de la empresa-->
                    <div class="col-md-6  {{ $errors->has('email_empresa') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="email_empresa">Email de la empresa <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       value="{{($empresa->CORRERO_ELECTRONICO)}}"
                                       name="email_empresa"
                                       id="email_empresa"
                                       type="email"
                                       placeholder="cargo@empresa.com">
                                {!! $errors->first('email_empresa', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-email-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Email de la empresa-->


                    <!-- Input Horario de atención de la empresa-->
                    <div class="col-md-12 {{ $errors->has('horario_empresa') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="horario_empresa">Horario de atención de la empresa <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       value="{{$empresa->HORARIO_ATENCION}}"
                                       type="text"
                                       name="horario_empresa"
                                       id="horario_empresa"
                                       placeholder="Lunes a viernes de 8:30 a 18:00 hrs.">
                                {!! $errors->first('horario_empresa', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-horario-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Horario de atención de la empresa-->
                </div>
                <!-- Select Sectores-->
                <div class="g-mb-30">
                    <label class="g-mb-10" for="select-sectores">Sector(es) <span class="g-color-red">*</span></label>
                </div>
                <!-- Fin Select Sectores-->

                <!-- Checkboxes Sectores-->
                <div class="row g-mb-30 container">
                    <?php $i = 0 ?>
                    @foreach($sectores as $sector)
                        <span id="sector-span-{{$i}}" hidden>{{$sector->ID_SECTOR}}</span>
                        <div class="col-md-4">
                            <div class="form-group g-mb-10">
                                <label class="u-check g-pl-25">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" {{ $sector->ACTIVO_INACTIVO == 1 ? 'checked=""' : '' }} id="sector-{{$i}}" type="checkbox">
                                    <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </div>
                                    {{$sector->DESCRIPCION}}
                                    <?php $i ++?>
                                </label>
                            </div>
                        </div>
                    @endforeach

                </div>
                <!-- Fin Checkboxes Sectores-->
            </div>
            <!-- Fin Información sobre la empresa -->
        </div>
        <!-- Domicilio la empresa -->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Domicilio de la empresa</h3>
                <div class="row">
                    <!-- Select Países-->
                    <div class="col-md-4">
                        <div class="g-mb-30">
                            <label class="g-mb-10" for="select-paises">País <span class="g-color-red">*</span></label>
                            <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                <i class="hs-admin-angle-down"></i>
                            </div>
                            <select id="select-paises" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" disabled>
                                <option value="1">México</option>
                            </select>
                        </div>
                    </div>
                    <!-- Fin Select Países-->
                    <!-- Select Estados-->
                    <div class="col-md-4">
                        <div class="g-mb-30">
                            <label class="g-mb-10" for="select-estados">Estado <span class="g-color-red">*</span></label>
                            <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                <i class="hs-admin-angle-down"></i>
                            </div>
                            <select id="select-estados" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">

                                @foreach($estados as $estado)

                                    <option value="{{$estado->ID_ESTADO}}">{{$estado->DESCRIPCION_ESTADO}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>
                    <!-- Fin Select Estados-->
                    <!-- Select Municipios-->
                    <div class="col-md-4">
                        <div class="g-mb-30">
                            <label class="g-mb-10" for="select-municipio">Municipio <span class="g-color-red">*</span></label>
                            <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                <i class="hs-admin-angle-down"></i>
                            </div>
                            <select id="select-municipio" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">

                                @foreach($municipios as $municipio)

                                    <option {{$empresa->ID_MUNICIPIO == $municipio->ID_MUNICIPIO ? 'selected' : ''}} value="{{$municipio->ID_MUNICIPIO}}">{{$municipio->DESCRIPCION}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>
                    <!-- Fin Select Municipios-->
                </div>
                <!-- Input Calle Domicilio -->
                <div class="form-group g-mb-30 {{ $errors->has('calle_domicilio') ? 'g-color-red': '' }}">
                    <label class="g-mb-10 g-color-black" for="calle_domicilio">Calle del domicilio <span class="g-color-red">*</span></label>
                    <div class="g-pos-rel">
                        <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                               value="{{$empresa->CALLE}}"
                               name="calle_domicilio"
                               id="calle_domicilio"
                               type="text"
                               placeholder="Ejemplo: Calle del Norte">
                        {!! $errors->first('calle_domicilio', '<br><span class="help-block">:message</span>') !!}
                        <div class="g-mt-5">
                            <span id="error-calle-domicilio" style="display: none" class="g-color-red"></span>
                        </div>
                    </div>
                </div>
                <!-- Fin Input Calle Domicilio -->
                <div class="row">
                    <!-- Input Número Exterior/Interior -->
                    <div class="col-md-4 {{ $errors->has('num_int_ext') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="num_int_ext">Número Exterior/Interior <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                       value="{{$empresa->NUM_INT}}"
                                       name="num_int_ext"
                                       id="num_int_ext"
                                       type="text"
                                       placeholder="Ejemplo: 100-1">
                                {!! $errors->first('num_int_ext', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-num-int-ext" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Número Exterior/Interior -->
                    <!-- Input Colonia -->
                    <div class="col-md-4 {{ $errors->has('colonia') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="colonia">Colonia <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input  class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                        value="{{$empresa->COLONIA}}"
                                        name="colonia"
                                        id="colonia"
                                        type="text"
                                        placeholder="Ejemplo: Centro">
                                {!! $errors->first('colonia', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-colonia" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Colonia -->
                    <!-- Input Código Postal -->
                    <div class="col-md-4  {{ $errors->has('codigo_postal') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="codigo_postal">Código Postal <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                       value="{{$empresa->CODIGO_POSTAL}}"
                                       name="codigo_postal"
                                       id="codigo_postal"
                                       type="number"
                                       placeholder="Ejemplo: 20358">
                                {!! $errors->first('codigo_postal', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-codigo-postal" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Código Postal -->
                </div>
                <!-- Input Link Google Maps -->
                <div class="form-group g-mb-30 {{ $errors->has('link_google_maps') ? 'g-color-red': '' }}">
                    <label class="g-mb-10 g-color-black" for="link_google_maps">Link de la empresa en Google Maps <span class="g-color-red">*</span></label>
                    <div class="g-pos-rel">
                        <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                               value="{{$empresa->LINK_GOOGLE_MAPS}}"
                               name="link_google_maps"
                               id="link_google_maps"
                               type="text"
                               placeholder="Ejemplo: https://goo.gl/maps/e1rP1SskS31ANbK57">
                        {!! $errors->first('link_google_maps', '<br><span class="help-block">:message</span>') !!}
                        <div class="g-mt-5">
                            <span id="error-link-google-maps" style="display: none" class="g-color-red"></span>
                        </div>
                    </div>
                </div>
                <!-- Fin Input Link Google Maps
                <form id="GMapGeocodingForm" class="g-mb-30">
                    <div class="input-group g-brd-primary--focus g-mb-30">
                        <input id="GMapGeocodingAddress" class="form-control rounded-0 u-form-control" type="text" placeholder="Enter address">

                        <div class="input-group-addon p-0 g-brd-primary">
                            <button class="btn rounded-0 btn-primary btn-md g-font-weight-700 g-font-size-14" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
                <div id="GMapGeocoding" class="js-g-map embed-responsive embed-responsive-21by9" data-lat="-12.043333" data-lng="-77.028333" data-zoom="14" data-geocoding="true" data-cords-target="#GMapGeocodingAddress" data-pin="true"data-pin-icon="/assets/global/img/index/index-pin-mapa.png"></div>
                 -->
            </div>
        </div>
        <!-- Fin Domicilio la empresa -->
        <!-- Detalles de la empresa -->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Detalles de la empresa</h3>

                <!-- Textarea Descripción breve de la empresa -->
                <div class="form-group g-mb-30 {{ $errors->has('descripcion_empresa') ? 'g-color-red': '' }}">
                    <label class="g-mb-10 g-color-black" for="descripcion_empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                    <textarea class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4"
                              value="{{old('descripcion_empresa')}}"
                              name="descripcion_empresa"
                              id="descripcion_empresa"
                              rows="3"
                    >{{$empresa->DESCRIPCION_BREVE}}</textarea>
                    {!! $errors->first('descripcion_empresa', '<br><span class="help-block">:message</span>') !!}
                    <div class="g-mt-5">
                        <span id="error-descripcion-empresa" style="display: none" class="g-color-red"></span>
                    </div>
                </div>
                <!-- Fin Textarea Descripción breve de la empresa -->


                <!-- File Input Brochure/Catálogo/Presentación de la empresa -->
                <div class="form-group mb-30">
                    <p class="g-brd-gray-light-v3 g-mb-20">Brochure/Catálogo/Presentación de la empresa </p>
                    <input id="input-file-presentacion-empresa" type="file">
                </div>
                <!-- Fin File Input Brochure/Catálogo/Presentación de la empresa -->

                <!-- File Input Logotipo de la empresa -->
                <div class="form-group mb-0">
                    <p class="g-brd-gray-light-v3  g-mb-20">Logotipo de la empresa <span class="g-color-red">*</span></p>
                    <input id="input-file-logo-empresa" type="file">
                </div>
                <!-- Fin File Input Logotipo de la empresa -->

            </div>
        </div>
        <!-- Fin Detalles de la empresa -->
        <!-- Certificaciones o reconocimientos -->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Certificaciones o reconocimientos</h3>

                <!-- Input Premios de calidad obtenidos -->
                <div class="form-group g-mb-30 {{ $errors->has('premios_obtenidos') ? 'g-color-red': '' }}">
                    <label class="g-mb-10 g-color-black" for="premios_obtenidos">Premios de calidad obtenidos </label>
                    <div class="g-pos-rel">
                        <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                               value="{{$empresa->PREMIOS_CALIDAD}}"
                               name="premios_obtenidos"
                               id="premios_obtenidos"
                               type="text"
                               placeholder="Escribe premios de calidad obtenidos">
                        {!! $errors->first('premios_obtenidos', '<br><span class="help-block">:message</span>') !!}
                        <div class="g-mt-5">
                            <span id="error-premios-obtenidos" style="display: none" class="g-color-red"></span>
                        </div>
                    </div>
                </div>
                <!-- Fin Input Premios de calidad obtenidos -->

                <!-- File Input documentos/comprobantes de premios o certificaciones obtenidas -->
                <div class="form-group mb-30">
                    <p class="g-brd-gray-light-v3 g-mb-20">Subir documentos/comprobantes de premios o certificaciones obtenidas </p>
                    <input id="input-file-logo-empresa" type="file">

                </div>
                <!-- Fin File Input documentos/comprobantes de premios o certificaciones obtenidas -->

                <label class="g-mb-30 g-mt-30">Certificaciones</label>

                <!-- Checkboxes Certificaciones-->
                <div class="row g-mb-30 container">
                    <?php $i = 0 ?>
                    @foreach($certificaciones as $certificacion)
                        <div class="col-md-4">
                            <span id="certificacion-span-{{$i}}" hidden>{{$certificacion->ID_CERTIFICACION}}</span>
                            <div class="form-group g-mb-10">
                                <label class="u-check g-pl-25">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" {{ $certificacion->ACTIVO_INACTIVO == 1 ? 'checked=""' : '' }}  id="certificacion-{{$i}}" type="checkbox">
                                    <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </div>
                                    {{$certificacion->DESCRIPCION}}
                                    <?php $i ++?>
                                </label>
                            </div>
                        </div>
                    @endforeach

                </div>
                <!-- Fin Checkboxes Certificaciones-->
            </div>
        </div>
        <!-- Información estadística de la empresa-->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Información estadística de la empresa</h3>

                <div class="row">

                    <!-- Input Número de empleados activos-->
                    <div class="col-md-6 {{ $errors->has('num_empleados') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="num_empleados">Número de empleados activos <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       name="num_empleados"
                                       id="num_empleados"
                                       value="{{$empresa->CANTIDAD_EMPLEADOS}}"
                                       type="number"
                                       placeholder="Incluyendo: hombres, mujeres, discapacitados">
                                {!! $errors->first('num_empleados', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-num-empleados" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Número de empleados activos-->

                    <!-- Input Crecimiento anual (porcentaje)-->
                    <div class="col-md-6 {{ $errors->has('crecimiento_anual') ? 'g-color-red': '' }}">
                        <label class="g-mb-10 g-color-black" for="crecimiento_anual">Crecimiento anual (porcentaje) <span class="g-color-red">*</span></label>
                        <div class="form-group g-mb-30 w-50">
                                        <span class="g-pos-abs g-top-0 d-block h-100" style="right: 45% !important;">
	                                        <i class="g-absolute-centered g-font-size-16 g-color-gray-light-v6">%</i>
	                                    </span>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10 d-lg-inline-block "
                                       name="crecimiento_anual"
                                       id="crecimiento_anual"
                                       value="{{$empresa->CRECIMIENTO_ANUAL}}"
                                       type="number"
                                       placeholder="Ejemplo: 20">
                                {!! $errors->first('crecimiento_anual', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-crecimiento-anual" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Crecimiento anual (porcentaje)-->

                    <!-- Input Ventas anuales -->
                    <div class="col-md-6 {{ $errors->has('ventas_anuales') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="ventas_anuales">Ventas anuales <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       name="ventas_anuales"
                                       id="ventas_anuales"
                                       value="{{$empresa->PROMEDIO_VENTA_ANUAL}}"
                                       type="number"
                                       placeholder="Cifra en números">
                                {!! $errors->first('ventas_anuales', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-ventas-anuales" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Ventas anuales-->

                    <!-- Input Patentes Registradas -->
                    <div class="col-md-6 {{ $errors->has('patentes_registradas') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="patentes_registradas">Patentes registradas <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       id="patentes_registradas"
                                       name="patentes_registradas"
                                       value="{{$empresa->PATENTES_REGISTRADAS}}"
                                       type="number"
                                       placeholder="Cifra en números">
                                {!! $errors->first('patentes_registradas', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-patentes-registradas" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Patentes Registradas-->

                </div>

            </div>
        </div>
        <!-- Fin Información estadística de la empresa -->
        <!-- Redes sociales de la empresa-->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Redes sociales de la empresa</h3>

                <div class="row">

                    <!-- Input Facebook -->
                    <div class="col-md-6 {{ $errors->has('facebook_empresa') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="facebook_empresa">Facebook</label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       name="facebook_empresa"
                                       id="facebook_empresa"
                                       value="{{$empresa->LINK_FACEBOOK}}"
                                       type="text"
                                       placeholder="Ejemplo: https://www.facebook.com/NombreDelaEmpresa/">
                                {!! $errors->first('facebook_empresa', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-facebook-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Facebook -->

                    <!-- Input Linkedin-->
                    <div class="col-md-6 {{ $errors->has('linkedin_empresa') ? 'g-color-red': '' }}">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10 g-color-black" for="linkedin_empresa">Linkedin</label>
                            <div class="g-pos-rel">
                                <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       name="linkedin_empresa"
                                       id="linkedin_empresa"
                                       value="{{$empresa->LINK_LINKEDIN}}"
                                       type="text"
                                       placeholder="Ejemplo: https://www.linkedin.com/company/NombreDelaEmpresa/">
                                {!! $errors->first('linkedin_empresa', '<br><span class="help-block">:message</span>') !!}
                                <div class="g-mt-5">
                                    <span id="error-linkedin-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input Linkedin-->

                    <!-- Input Youtube -->
                    <div class="col-md-6 ">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10" for="input-youtube">Youtube</label>
                            <div class="g-pos-rel">
                                <input id="youtube_empresa"
                                       name="youtube_empresa"
                                       value="{{$empresa->LINK_YOUTUBE}}"
                                       class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       type="text"
                                       placeholder="Ejemplo: https://www.youtube.com/channel/UC5Kh2XFPylegBW_zDJ1jj3w">
                                <div class="g-mt-5">
                                    <span id="error-youtube-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input -->

                    <!-- Input Twitter -->
                    <div class="col-md-6 ">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10" for="input-youtube">Twitter</label>
                            <div class="g-pos-rel">
                                <input id="twitter_empresa"
                                       name="twitter_empresa"
                                       value="{{$empresa->LINK_TWITTER}}"
                                       class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       type="text"
                                       placeholder="Ejemplo: https://www.youtube.com/channel/UC5Kh2XFPylegBW_zDJ1jj3w">
                                <div class="g-mt-5">
                                    <span id="error-twitter-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input -->

                    <!-- Input Instagram -->
                    <div class="col-md-6 ">
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10" for="input-youtube">Instagram</label>
                            <div class="g-pos-rel">
                                <input id="instagram_empresa"
                                       name="instagram_empresa"
                                       value="{{$empresa->LINK_INSTAGRAM}}"
                                       class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                       type="text"
                                       placeholder="Ejemplo: https://www.youtube.com/channel/UC5Kh2XFPylegBW_zDJ1jj3w">
                                <div class="g-mt-5">
                                    <span id="error-instagram-empresa" style="display: none" class="g-color-red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Input -->

                </div>

            </div>
        </div>
        <!-- Fin Redes sociales de la empresa-->

        <!-- Productos / Servicios / Procesos de la empresa -->
        <div class="col-md-12">
            <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Productos / Servicios / Procesos de la empresa</h3>

                <div class="row">

                    <!-- Panel Agregar nuevo -->
                    <div class="col-md-5">

                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center g-font-size-12 g-font-size-default--md g-color-black g-mb-20">
                                <span class="g-color-blue fa fa-plus-circle g-mr-7"></span> Agregar Nuevo</h3>

                            <center>
                                <!-- Foto de Servicio -->
                                <div class="u-block-hover">
                                    <figure>
                                        <img src="/assets/global/img/servicios/servicio-estampado-cnc.jpeg" height="150">
                                    </figure>

                                    <!-- Ajustes de foto de perfil -->
                                    <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                                        <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                            <!-- Iconos de ajustes de foto de perfil -->
                                            <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                                <li class="list-inline-item align-middle g-mx-7">
                                                    <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                                        <i class="fa fa-edit u-line-icon-pro" alt="Cambiar Foto"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!-- Fin Iconos de Ajustes de foto de perfil -->
                                        </div>
                                    </figcaption>
                                    <!-- Fin Ajustes de foto de perfil-->

                                </div>
                                <!-- Fin Foto de Perfil -->
                            </center>

                            <!-- Input Nombre Servicio Autocompletado -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="input-nombre-servicio-auto">Nombre <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input id="input-nombre-servicio-auto" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" value="Estampado CNC">
                                </div>
                            </div>
                            <!-- Fin Input Nombre Servicio Autocompletado -->

                            <!-- Textarea Descripción breve de la empresa -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="txta-descripcion-empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                                <textarea id="txta-descripcion-empresa" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" disabled
                                          rows="8">Nuestras máquinas CNC para corte y estampado de precisión son ideales para series de producción pequeñas y medianas, mejor adaptadas a los productos personalizados que el mercado demanda y de menor costo.</textarea>
                            </div>
                            <!-- Fin Textarea Descripción breve de la empresa -->

                            <center>
                                <a href="#!" class="btn btn-md u-btn-blue g-mr-10 g-mb-15">Guardar</a>
                            </center>

                        </div>

                    </div>
                    <!-- Fin Panel Agregar nuevo -->

                    <!-- Tabla Ítems Agregados -->
                    <div class="col-md-7">

                        <!-- Basic Table -->
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">

                            <h3 class="d-flex align-self-center g-font-size-12 g-font-size-default--md g-color-black g-mb-20">
                                <span class="fa fa-list g-mr-7"></span> Ítems Agregados</h3>

                            <div class="table-responsive">
                                <table class="table u-table--v1 mb-0">
                                    <thead>
                                    <tr>
                                        <th>Foto</th>
                                        <th class="hidden-sm">Nombre</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td><img src="/assets/global/img/servicios/servicio-doblado-de-tubo.jpeg" width="60"></td>
                                        <td>Doblado de tubo</td>
                                        <td>
                                            <a href="javascript:;" data-toggle="modal" data-target="#modalServicio" class="btn g-font-size-16 u-btn-yellow fa fa-edit g-color-black"></a>
                                        </td>
                                        <td>
                                            <a href="#!" class="btn g-font-size-16 u-btn-red fa fa-trash"></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Basic Table -->
                    </div>
                    <!-- Fin abla Ítems Agregados -->

                </div>

            </div>
        </div>
        <!-- Fin Productos / Servicios / Procesos de la empresa -->

        <!-- Modal que contiene un formulario para editar servicios agregados -->
        <div class="modal fade" id="modalServicio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <center>
                            <!-- Foto de Servicio -->
                            <div class="u-block-hover">
                                <figure>
                                    <img src="/assets/global/img/servicios/servicio-doblado-de-tubo.jpeg" height="150">
                                </figure>

                                <!-- Ajustes de foto de perfil -->
                                <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                                    <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                        <!-- Iconos de ajustes de foto de perfil -->
                                        <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                            <li class="list-inline-item align-middle g-mx-7">
                                                <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                                    <i class="fa fa-edit u-line-icon-pro" alt="Cambiar Foto"></i>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- Fin Iconos de Ajustes de foto de perfil -->
                                    </div>
                                </figcaption>
                                <!-- Fin Ajustes de foto de perfil-->

                            </div>
                            <!-- Fin Foto de Perfil -->
                        </center>

                        <!-- Input Nombre Servicio Autocompletado -->
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10" for="input-nombre-servicio-auto">Nombre <span class="g-color-red">*</span></label>
                            <div class="g-pos-rel">
                                <input onfocus="search_bar_modal();" id="input-nombre-modal-servicio-auto" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" value="Doblado de tubo">
                            </div>
                        </div>
                        <!-- Fin Input Nombre Servicio Autocompletado -->

                        <!-- Textarea Descripción breve de la empresa -->
                        <div class="form-group g-mb-30">
                            <label class="g-mb-10" for="txta-descripcion-empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                            <textarea id="txta-descripcion-empresa" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" disabled
                                      rows="8">Nuestras máquinas CNC para corte y estampado de precisión son ideales para series de producción pequeñas y medianas, mejor adaptadas a los productos personalizados que el mercado demanda y de menor costo.</textarea>
                        </div>
                        <!-- Fin Textarea Descripción breve de la empresa -->

                        <center>
                            <a href="#!" class="col-4 g-py-10 btn btn-md u-btn-yellow g-mr-10 g-mb-15 g-color-black" data-dismiss="modal" aria-label="Close">Cancelar</a>
                            <a href="#!" class="col-7 g-py-10 btn btn-md u-btn-blue g-mr-10 g-mb-15" data-dismiss="modal" aria-label="Close">Guardar Cambios</a>
                        </center>

                    </div>
                </div>
            </div>
        </div>
        <!-- Fin Modal que contiene un formulario para editar servicios agregados -->
    @endforeach
@endif